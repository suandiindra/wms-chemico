<?php
  include('utility/config.php');
  include('utility/fungsi.php');
  session_start();
  if(empty($_SESSION['user_id'])){
    echo "<script>window.location='login.php'</script>";
  }
  $jab_id = $_SESSION['m_jabatan_id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/pavicon.jpeg" rel="icon">
  <title>WMS</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css">
</head>
<script type="text/javascript">
    $(document).ready(function(){
        $('#kodebarang').on('change',function(){
        var kodebarang = $(this).val();
        if(kodebarang){
            $.ajax({
                type:'POST',
                url:'pages/Keluar/ajaxData.php',
                data:'kodebarang='+kodebarang,
                success:function(html){
                    $('#batch').html(html);
                    $('#rak').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#batch').html('<option value="">Select country first</option>');
            $('#rak').html('<option value="">Select state first</option>'); 
        }
    });

    $('#batch').on('change',function(){
        var batch = $(this).val();
        if(batch){
            $.ajax({
                type:'POST',
                url:'pages/Keluar/ajaxData.php',
                data:'batch='+batch,
                success:function(html){
                    $('#rak').html(html);
                }
            }); 
        }else{
            $('#rak').html('<option value="">Select state first</option>'); 
        }
    });
});
</script>
<style>
::-webkit-input-placeholder { /* Edge */
  color: Blue;
}
.padding-table-columns td
{
    padding:0 55px 0 0; /* Only right padding*/
}
:-ms-input-placeholder { /* Internet Explorer */
  color: Blue;
}

::placeholder {
  color: Blue;
}

.chosen-container-single .chosen-single {
    height: 40px;
    width : 260px;
    border-radius: 3px;
    border: 1px solid #CCCCCC;
}
.chosen-container-single .chosen-single span {
    padding-top: 7px;
}
.chosen-container-single .chosen-single div b {
    margin-top: 2px;
}

</style>
<body id="page-top">
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion"  id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" style="background-color:white" href="index.html">
        <div class="sidebar-brand-icon">
          <img src="img/logopt.jpeg" style="padding-left:0px; width:90%">
        </div>
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item">
        <a class="nav-link" href="./?route=profile">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span> <b> <?php echo " ".$_SESSION['jabatan_desc']." "; ?></span></b></a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Navigation Menu
      </div>
      <?php 
          
          // echo $sel;
          // $selheader = "select * from m_menu_master";
          $selheader = "select a.m_menu_master_id, a.nama_master,a.master_menu_icon from m_menu_master a
          inner join m_menu b on a.m_menu_master_id = b.m_menu_master_id
          inner join r_user_menu c on c.m_menu_id = b.m_menu_id
          where c.m_jabatan_id = '$jab_id'
          group by a.m_menu_master_id, a.nama_master,a.master_menu_icon";
          $res = mysqli_query($con,$selheader);
          while($dt = mysqli_fetch_array($res)){
          ?>
          <li class="nav-item">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#<?php echo $dt["nama_master"] ?>"
                aria-expanded="true" aria-controls="<?php echo $dt["nama_master"] ?>">
                <i class="<?php echo $dt["master_menu_icon"]; ?>"></i>
                <span><?php echo $dt["nama_master"] ?></span>
              </a>
              <div id="<?php echo $dt["nama_master"] ?>" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header"><?php echo $dt["nama_master"] ?></h6>
                  <hr>
                  <?php
                      $master_menu_id = $dt["m_menu_master_id"];
                      $sel_dt = "select * from r_user_menu b 
                      inner join m_menu c on c.m_menu_id = b.m_menu_id
                      where b.m_jabatan_id = '$jab_id' and c.m_menu_master_id = '$master_menu_id'";
                      // echo $sel_dt;
                      $res_menu_dtl = mysqli_query($con,$sel_dt);
                      while($dt_sub = mysqli_fetch_array($res_menu_dtl)){
                  ?>
                      <a class="collapse-item" href="<?php echo './'.$dt_sub['menu_url']; ?>"><?php echo $dt_sub['menu_desc'];  ?></a>
                  <?php
                      }
                  ?>
                </div>
              </div>
          </li>
          <?php
          }
      ?>
     
      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link" href="./logout.php">
          <i class="fa fa-fw fa-times"></i>
          <span>LogOut</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <div class="version" id="Chemico Surabaya WMS"></div>
    </ul>
    <!-- Sidebar -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <!-- TopBar -->
        <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
          <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="./?route=profile" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle" src="img/boy.png" style="max-width: 60px">
                <span class="ml-2 d-none d-lg-inline text-white small"><?php echo $_SESSION['user_id'] ?></span>
              </a>
            </li>
          </ul>
        </nav>
        <!-- Topbar -->
        <!-- Container Fluid-->
        <?php
            include('content.php');
        ?>
        <!---Container Fluid-->
      </div>

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script> - developed by PT.CHEMICO SURABAYA
             
            </span>
          </div>
        </div>
      </footer>
      <!-- Footer -->
    </div>
  </div>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>
  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script>
    $(document).ready(function () {
      $('#dataTable').DataTable(); // ID From dataTable 
      $('#dataTableHover').DataTable(); // ID From dataTable with Hover
    });
  </script>
  <script>
    $('#exampleModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text(recipient)
    modal.find('.modal-body input').val(recipient)
    })
</script>
</body>

</html>