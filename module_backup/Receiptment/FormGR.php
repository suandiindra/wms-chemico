<?php
    $m_user_id  = $_SESSION['user_id'];
    $t_po_id = "";
    $act = "";
    $novceck  = "";
    $nopo = "";
    $tglpo = "";
    $vendor = "";
    $created = "";
    $grstatus = "";
    if(isset($_POST['caripo'])){
        if(isset($_POST['randcheck'])){
            $id = $_POST['nopo'];
            $caripo= "select a.t_po_id,a.nomor_po,a.nomor_visual_check,a.tgl_po,d.principle_desc,e.m_vendor_id,e.vendor_desc,a.status_po,c.user_name
            ,b.m_barang_id,b.nama_barang,b.qty,a.careated_by,ifnull(f.`Status`,'Belum Di konfirmasi') as grstatus
            from t_po a
            inner join t_po_detail b on a.t_po_id = b.t_po_id
            inner join m_user c on c.user_name = a.careated_by
            left join m_principle d on d.m_principle_id = a.m_principle_id
            left join t_gr f on f.t_po_id = a.t_po_id
            left join m_vendor e on e.m_vendor_id = a.m_vendor_id where a.nomor_po = '$id'";

            //echo $caripo;
            $res = mysqli_query($con,$caripo);
            $dt = mysqli_fetch_array($res);
            $novceck = $_POST['novceck'];
            $nopo = $dt['nomor_po'];
            $tglpo = $dt['tgl_po'];
            $vendroid = $dt['m_vendor_id'];
            $vendor = $dt['vendor_desc'];
            $created = $dt['careated_by'];
            $t_po_id = $dt['t_po_id'];
            $grstatus = $dt['grstatus'];
        }
    }

    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $caripo= "select a.t_po_id,a.nomor_po,a.nomor_visual_check,a.tgl_po,d.principle_desc,e.m_vendor_id,e.vendor_desc,a.status_po,c.user_name
        ,b.m_barang_id,b.nama_barang,b.qty,a.careated_by,ifnull(f.`Status`,'Belum Di konfirmasi') as grstatus
        from t_po a
        inner join t_po_detail b on a.t_po_id = b.t_po_id
        inner join m_user c on c.user_name = a.careated_by
        left join m_principle d on d.m_principle_id = a.m_principle_id
        left join t_gr f on f.t_po_id = a.t_po_id
        left join m_vendor e on e.m_vendor_id = a.m_vendor_id where a.nomor_po = '$id'";

        //echo $caripo;
        $res = mysqli_query($con,$caripo);
        $dt = mysqli_fetch_array($res);
        $novceck = $_GET['novceck'];
        $nopo = $dt['nomor_po'];
        $tglpo = $dt['tgl_po'];
        $vendroid = $dt['m_vendor_id'];
        $vendor = $dt['vendor_desc'];
        $created = $dt['careated_by'];
        $t_po_id = $dt['t_po_id'];
        $grstatus = $dt['grstatus'];
    }
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Form Penerimaan Barang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Edit PO</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
    <form action="" method="POST">
        <div class="row">
            <div class="col-lg-8">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <input type="text" value="<?php echo $nopo ?>" placeholder="Masukan Nomor PO" name="nopo" required class="form-control" id="exampleInputFirstName" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" id = "vno" value="<?php echo $novceck ?>" onchange="vno()" placeholder="Virtual Check Material Nomor" required name="novceck" class="form-control" id="exampleInputFirstName" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                               <button class="btn btn-success" name="caripo">Periksa PO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <!-- fetch data here -->
        <b>
        <div class="form-group">
        <hr>    
            <table class="padding-table-columns">
                <tr>
                    <td ><label for="">Tgl PO</label></td>
                    <td ><label for="">:</label></td>
                    <td ><label for=""><?php echo $tglpo; ?></label></td>
                </tr>
                <tr>
                    <td ><label for="">Vendor</label></td>
                    <td ><label for="">:</label></td>
                    <td ><label for=""><?php echo $vendor; ?></label></td>
                </tr>
                <tr>
                    <td ><label for="">Dibuat Oleh</label></td>
                    <td ><label for="">:</label></td>
                    <td ><label for=""><?php echo $created; ?></label></td>
                </tr>
                <tr>
                    <td ><label for="">Status</label></td>
                    <td ><label for="">:</label></td>
                    <td ><label for=""><?php echo $grstatus; ?></label></td>
                </tr>
            </table>
        </div>
        </b>
        <!-- sampai sini -->
        <hr>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Nama Item</th>
            <th>QTY PO</th>
            <th>QTY Terima</th>
            <th>Sisa PO</th>
            <th>Satuan</th>
            <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $sel = "select b.*,cast(ifnull(c.qtygr,ifnull(qtyterima,0)) as float) qtygr,cast(b.qty - ifnull(qtygr,ifnull(qtyterima,0)) as float) as qtysisa from t_po a
                        inner join t_po_detail b on a.t_po_id = b.t_po_id
                        left join (SELECT t_po_id,m_barang_id,cast(sum((qty_gr)) as float) qtygr from t_gr a
                                    inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
                                    group by t_po_id,m_barang_id) c on c.t_po_id = a.t_po_id and b.m_barang_id = c.m_barang_id
                        left join (select t_po_id,m_barang_id,sum((qty))as qtyterima from t_gr_detail_temp where usercreated = '$m_user_id'
                                            group by t_po_id,m_barang_id) d on d.t_po_id = a.t_po_id and b.m_barang_id =  d.m_barang_id
                        where a.t_po_id = '$t_po_id'";

                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
            ?>
            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['qty']; ?></td>
            <td><?php echo $res['qtygr']; ?></td>
            <td><?php echo $res['qtysisa']; ?></td>
            <td><?php echo "Kg" ?></td>
            <td style="text-align:center">
                <a href="./?route=transferbarang&t_po_id=<?php echo $t_po_id ?>&nopo=<?php echo $nopo ?>&kodebarang=<?php echo $res['m_barang_id'] ?>&vcek=<?php echo $novceck;?>"><button class="btn btn-danger">Terima</button></a>
                 <!-- <button class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" 
                 data-whatever="<?php echo $res['m_barang_id']."#".$res['nama_barang']."#".$res['qty']; ?>">Transfer >></button> -->
            </td>
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
    </div>
    <a href="./pages/Receiptment/actiongr.php?act=conf&t_po_id=<?php echo $t_po_id; ?>&vcheck=<?php echo $novceck ?>">
    <button class="btn btn-success">Konfirmasi Penerimaan</button>
    </a>
</div>