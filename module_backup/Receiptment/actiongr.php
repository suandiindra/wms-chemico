<?php
    session_start();
   
    $m_user_id = $_SESSION['user_id'];
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    if(isset($_GET['act'])){
        $nopo = $_GET['t_po_id'];
        $novceck = $_GET['vcheck'];

        //1. ambil table temp GR detai where nopo dan vcheck
        $sel0 = "select sum(qty)as qty from t_gr_detail_temp where t_po_id = '$nopo'";
        $res = mysqli_query($con,$sel0);
        $dt0 = mysqli_fetch_array($res);
        $qtyGR  = $dt0['qty'];


        $sel1 = "select sum(qty) as qty_po from t_po a
        inner join t_po_detail b on a.t_po_id = b.t_po_id
        where a.t_po_id = '$nopo'";
        $res = mysqli_query($con,$sel1);
        $dt1 = mysqli_fetch_array($res);
        $qtyPO  = $dt1['qty_po'];

        $selvcek = "select count(*)cekgr from t_gr where visual_check_no = '$novceck'";
        $rw = mysqli_fetch_array(mysqli_query($con,$selvcek));
        if((int) $rw['cekgr'] > 0){
            echo "<script>alert('Gunakan Nomor Visual Cek lain...')</script>";
            echo "<script>window.location = '../../?route=GR'</script>";
            exit;
        }

        if((float) $qtyGR <= (float) $qtyPO){
            $t_gr_id = uniq();
            if((float) $qtyGR <= (float) $qtyPO){
                $stat = "Outstanding";
            }else{
                $stat = "Complete";
            }

            $insertH = "insert into t_gr (t_gr_id,tgl_gr,t_po_id,visual_check_no,`Status`,penerima,createdate)
            values ('$t_gr_id',now(),'$nopo','$novceck','1','$m_user_id',now())";

            $insertD = "insert into t_gr_detail (t_gr_id,m_barang_id,qty_gr,batch,nopack,packsize,expired,m_rak_id,remark,createdate)
            select '$t_gr_id',m_barang_id,qty,batch,nopack,packsize,expired,m_rak_id,remark,now() 
            from t_gr_detail_temp where t_po_id = '$nopo' and vcheck = '$novceck'";

            

            $deltemp = "delete from t_gr_detail_temp where t_po_id = '$nopo' and vcheck = '$novceck'";

            mysqli_query($con,$insertH);
            mysqli_query($con,$insertD);
            mysqli_query($con,$deltemp);
            // echo $insertH ."<br>";
            // echo $insertD ."<br>";
            // echo $updatePO ."<br>";
            // echo $deltemp ."<br>";
            
            
            $cekstok = "select m_barang_id,batch,m_rak_id,sum(cast(qty_gr as float))as qty,max(expired) expired  from t_gr_detail 
                        where t_gr_id = '$t_gr_id' 
                        group by m_barang_id,batch,m_rak_id";
            // echo $cekstok;
            $res = mysqli_query($con,$cekstok);
            while($dt = mysqli_fetch_array($res)){
                $m_barang_id = $dt['m_barang_id'];
                $qty = $dt['qty'];
                $batch = $dt['batch'];
                $m_rak_id = $dt['m_rak_id'];
                $expired = $dt['expired'];

                $selstok = "select count(*)jml from t_stok where m_barang_id = '$m_barang_id' 
                and batch = '$batch' and m_rak_id = '$m_rak_id' ";
                $exsist = mysqli_fetch_array(mysqli_query($con,$selstok));
                // echo $selstok;
                if((int)$exsist['jml'] == 0){
                    $stokid = uniq();
                    $insertstok = "insert into t_stok select '$stokid','$m_barang_id','$batch','$expired','$m_rak_id','0','$qty','0','0','0','$qty',now()";

                    mysqli_query($con,$insertstok);
                }else{
                    $updatestok = "update t_stok set qty_masuk = TRUNCATE(cast(qty_masuk as float) + cast('$qty' as float),2)  
                    ,last_stok = TRUNCATE(cast(last_stok as float) + cast('$qty' as float) ,2)
                    ,last_update = now() 
                    where m_barang_id = '$m_barang_id' and batch = '$batch' and m_rak_id = '$m_rak_id'";
                    mysqli_query($con,$updatestok);
                }

            }
            $sts = cekstatusPO($con,$nopo);
            $updatePO = "update t_po set status_po = '$sts' where t_po_id = '$nopo'";
            mysqli_query($con,$updatePO);

        }else{
            echo "<script>alert('Lebih besar dari PO')</script>";
        }
        echo "<script>window.location = '../../?route=GR'</script>";
        
    }

?>