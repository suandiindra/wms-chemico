<!DOCTYPE html>
<html>
<head>
  <title>PT. CHEMICO SURABAYA</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body onload="window.print()">
<div class="d-flex flex-row justify-content-center align-items-center" style="height: 70px;">
    <div class="p-2">
        <h4>PENERIMAAN BARANG</h4>
    </div>
</div>
<?php
    include("../../utility/config.php");
    $id = $_GET['id'];
    $vcek = $_GET['vcek'];
    $sel = "select a.t_gr_id,d.nomor_po,visual_check_no,tgl_gr,e.vendor_desc,
    c.nama_barang, batch,expired,b.packsize,cast(qty_gr as float)qty_gr,remark,a.penerima
    from t_gr a
    inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
    inner join m_barang c on c.m_barang_id = b.m_barang_id
    inner join t_po d on d.t_po_id = a.t_po_id
    inner join m_vendor e on e.m_vendor_id = d.m_vendor_id
    where a.t_gr_id = '$id' ";

    // echo $sel;
    $res = mysqli_query($con,$sel);
    $dt = mysqli_fetch_array($res);
?>
<div class="d-flex flex-row align-items-left" >
    <img src="../../img/chemico.jpeg" style="width:130px;margin-top:-50px; margin-left:20px;height:100px" alt="">
</div>
<b> PT. CHEMICO SURABAYA</b>
<hr>
<div class="form-row col-md-6">
    <div class="col">
        <table>
            <tr>
                <td>Nomor Penerimaan</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><?php echo $dt['visual_check_no'] ?></td>
            </tr>
            <tr>
                <td>Nomor PO</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><?php echo $dt['nomor_po'] ?></td>
            </tr>
            <tr>
                <td>Tgl Penerimaan</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><?php echo $dt['tgl_gr'] ?></td>
            </tr>
            <tr>
                <td>Supplier</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><?php echo $dt['vendor_desc'] ?></td>
            </tr>
        </table>
    </div>
</div>
<div class="form-row" style="margin-top:25px">
    <table class="table table-bordered">
    <thead>
        <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Barang</th>
        <th scope="col">QTY(Kg)</th>
        <th scope="col">Unit</th>
        <th scope="col">Batch</th>
        <th scope="col">Expired</th>
        <th scope="col">Remark</th>
        </tr>
    </thead>
    <?php
        $sel = "select a.t_gr_id,d.nomor_po,visual_check_no,tgl_gr,e.vendor_desc,
        c.nama_barang, batch,expired,b.packsize,cast(qty_gr as float)qty_gr,remark,a.penerima
        from t_gr a
        inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id
        inner join t_po d on d.t_po_id = a.t_po_id
        inner join m_vendor e on e.m_vendor_id = d.m_vendor_id
        where a.t_gr_id = '$id'";
    
        // echo $sel;
        $res1 = mysqli_query($con,$sel);
        $i = 1;
        while($dt = mysqli_fetch_array($res1)){
    ?>    
    <tbody>
        <tr>
        <th scope="row"><?php echo $i; ?></th>
        <td><?php echo $dt['nama_barang']; ?></td>
        <td><?php echo $dt['qty_gr']; ?></td>
        <td><?php echo $dt['packsize']; ?></td>
        <td><?php echo $dt['batch']; ?></td>
        <td><?php echo $dt['expired']; ?></td>
        <td><?php echo $dt['remark']; ?></td>
        </tr>
    </tbody>
    <?php
        $i = $i +1;
        }
    ?>  
    </table>
</div>
          <?php
            $currentDateTime = date('Y-m-d');
            echo $currentDateTime;
          ?>
    <div class="form-row">
        <div class="col" style="margin-left:10px">
          Truck Number
        </div>
    </div>
    <br>
    <div class="form-row">
        <div class="col" style="margin-left:10px">
          ____________________________
        </div>
    </div>
    <div class="form-row">
        <div class="col" style="margin-left:10px">
          Driver
        </div>
    </div>
    <br>
    <div class="form-row">
        <div class="col" style="margin-left:10px">
          ____________________________
        </div>
    </div>
    <div class="form-row">
        <div class="col" style="margin-left:10px">
          No Kendaraan
        </div>
    </div>
    <br>    
    <div class="form-row">
        <div class="col" style="margin-left:10px">
          ____________________________
        </div>
    </div>
    <div class="form-row">
        <div class="col" style="margin-left:10px">
         Catatan
        </div>
    </div>
    <br>    
    <div class="form-row">
        <div class="col" style="margin-left:10px">
          ____________________________________________________________________________________________________________
        </div>
    </div>
</body>