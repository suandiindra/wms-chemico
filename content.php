<?php
$path = "";

if(isset($_GET['route'])){
    $path = $_GET['route'];
}

// if($_SESSION['group_id'] == "01" && $path == ""){
//     $path = "listkendaraan";
// }else if($_SESSION['group_id'] == "04" && $path == ""){
//     $path = "listpelanggar";
// }

$m_user_id = $_SESSION['user_id'];
$cekGR = "select a.t_gr_detail_temp_id, nama_barang,principle_desc,batch,packsize,nopack,Expired,qty,rak_desc from t_gr_detail_temp a
inner join m_barang b on a.m_barang_id = b.m_barang_id
inner join m_principle d on d.m_principle_id = b.m_principle_id
inner join m_rak c on c.m_rak_id = a.m_rak_id
where a.usercreated = '$m_user_id'";
$cekDO = "select * from t_do_detail_temp where usercreated = '$m_user_id'";
$cekmutasi = "select * from t_mutasi where usercreated = '$m_user_id' and isconfirm = 0";
$cekadjustment = "select * from t_adjustment where usercreated = '$m_user_id' and isconfirm = 0";

$result = mysqli_query($con,$cekGR);
$resultDO = mysqli_query($con,$cekDO);
$resultMutasi = mysqli_query($con,$cekmutasi);
$resultadj = mysqli_query($con,$cekadjustment);


$pending_gr = mysqli_num_rows($result);
$pending_do = mysqli_num_rows($resultDO);
$pending_mutasi = mysqli_num_rows($resultMutasi);
$pending_adj = mysqli_num_rows($resultadj);

if($path != "tambahadj" && $pending_adj > 0){
    echo "<script>alert('Dokumen Adjustment gantung')</script>";
    include("pages/Adjustment/tambahadj.php");
    return;
}

if($path != "tambahgr" && $pending_gr > 0){
    echo "<script>alert('Dokumen Receiptment gantung')</script>";
    include("pages/Receiptment/FormGR.php");
    return;
}

if($path != "tambahdo" && $pending_do > 0){
    echo "<script>alert('Dokumen Delivery Order gantung')</script>";
    include("pages/Keluar/FormDO.php");
    return; 
}

if($path != "tambahmutasi" && $pending_mutasi > 0){
    echo "<script>alert('Dokumen Mutasi gantung')</script>";
    include("pages/Mutasi/formMutasi.php");
    return; 
}

if($path == "user"){
    include("pages/Master/user.php");
}else if($path == "barang"){
    include("pages/Master/barang.php");
}else if($path == "principal"){
    include("pages/Master/principle.php");
}else if($path == "vendor"){
    include("pages/Master/vendor.php");
}else if($path == "customer"){
    include("pages/Master/customer.php");
}else if($path == "input_po"){
    include("pages/Transaksi PO/Listpo.php");
}else if($path == "tambahpo"){
    include("pages/Transaksi PO/Formpo.php");
}else if($path == "editpo"){
    include("pages/Transaksi PO/Formpoedit.php");
}elseif($path == "GR"){
    include("pages/Receiptment/List.php");
}elseif($path == "tambahgr"){
    include("pages/Receiptment/FormGR.php");
}elseif($path == "transferbarang"){
    include("pages/Receiptment/transferbarang.php");
}elseif($path == "Brang_out"){
    include("pages/Keluar/List.php");
}elseif($path == "tambahdo"){
    include("pages/Keluar/FormDO.php");
}elseif($path == "mutasi"){
    include("pages/Mutasi/List.php");
}elseif($path == "tambahmutasi"){
    include("pages/Mutasi/formMutasi.php");
}elseif($path == "detailmutasi"){
    include("pages/Mutasi/Listdetailmutasi.php");
}elseif($path == "stok"){
    include("pages/Stok/List.php");
}elseif($path == "stokmap"){
    include("pages/Stok/stokmutasi.php");
}elseif($path == "adjustment"){
    include("pages/Adjustment/List.php");
}elseif($path == "tambahadj"){
    include("pages/Adjustment/tambahadj.php");
}elseif($path == "adjdetail"){
    include("pages/Adjustment/adjustmentdetail.php");
}elseif($path == "rak"){
    include("pages/Master/rak.php");
}elseif($path == "keluardetail"){
    include("pages/Keluar/keluardetail.php");
}elseif($path == "tukardetail"){
    include("pages/Keluar/tukardetail.php");
}elseif($path == "grdetail"){
    include("pages/Receiptment/grdetail.php");
}elseif($path == "bylocation"){
    include("pages/Laporan/itembylocation.php");
}elseif($path == "bymovementrak"){
    include("pages/Laporan/movementrak.php");
}elseif($path == "retur"){
    include("pages/Retur/list.php");
}elseif($path == "returform"){
    include("pages/Retur/FormRetur.php");
}elseif($path == "expired"){
    include("pages/Laporan/Expired.php");
}elseif($path == "gudang"){
    include("pages/Master/gudang.php");
}elseif($path == "kategori"){
    include("pages/Master/kategori.php");
}elseif($path == "returdetail"){
    include("pages/Retur/returdetail.php");
}elseif($path == "tukar"){
    include("pages/Laporan/penukaran.php");
}elseif($path == "profile"){
    include("pages/master/profile.php");
}elseif($path == "item"){
    include("pages/Laporan/itemmovement.php");
}


?>