<?php
    include('utility/config.php');
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <title>RuangAdmin - Login</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">
</head>
<style>
    .center-div {
        display: flex;
        flex-direction: column;
        justify-content: center;
        /* align-items: center; */
        /* text-align: center; */
        min-height: 100vh;
        }
</style>
<?php
    if(isset($_POST['id'])){
        $id     = $_POST['id'];
        $pass   = md5($_POST['pass']);
        // $pass = md5($_POST['pass']);
        $result = mysqli_query($con,"select *
                from m_user a
                inner join m_jabatan e on e.m_jabatan_id = a.m_jabatan_id
                where user_name = '$id' and password = '$pass'");
        if( mysqli_num_rows($result) > 0){
            $data = mysqli_fetch_array($result);
            if($id == $data["user_name"]){
                $_SESSION['user_id'] = $data["user_name"];
                $_SESSION['m_jabatan_id'] = $data["m_jabatan_id"];
                $_SESSION['jabatan_desc'] = $data["jabatan_desc"];
                echo "<script>window.location='index.php'</script>";
            }
            
        }else{
            echo "<script>alert('Gagal Login')</script>";
        }
    }
    

?>

<body class="bg-gradient-login">
  <!-- Login Content -->
  <div class= "center-div">
        <div class="container-login" >
            <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">
                <div class="card shadow-sm my-5">
                <div class="card-body p-0">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="login-form">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Login System</h1>
                        </div>
                        <form class="user" action="" method="POST">
                            <div class="form-group">
                            <input name="id" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp"
                                placeholder="User Name">
                            </div>
                            <div class="form-group">
                            <input type="password" name="pass" class="form-control" id="exampleInputPassword" placeholder="Password">
                            </div>
                            <div class="form-group">
                            </div>

                            <div class="form-group">
                            <!-- <a href="index.html" class="btn btn-primary btn-block">Login</a> -->
                            <button type="submit" class="btn btn-primary btn-block">Logins</button>
                            </div>
                            
                        </form>
                        <hr>
                        <div class="text-center">
                            
                        </div>
                        <div class="text-center">
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
  </div>
  <!-- Login Content -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>
</body>

</html>