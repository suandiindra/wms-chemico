<?php
     session_start();
     $m_user_id = $_SESSION['user_id'];
     include("../../utility/config.php");
     include("../../utility/fungsi.php");
    $where = $_GET['query'];
    echo "<script>window.location='../../?route=expired'</script>";
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Lap Item Expired.xls");
    $mydate=getdate(date("U"));
    $tgl = "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year]";
?>
<table>
    <tr>
        <td><h3><b>PT. Chemico Surabaya</b></h3></td>
    </tr>
    <tr>
        <td><b>Lap Item Expired</b></td>
    </tr>
    <tr>
        <td><b><?php echo $tgl; ?></b></td>
    </tr>
</table>
<table border=1>
<thead class="thead-light">
    <tr>
    <th>No.</th>
    <th>Nama Barang</th>
    <th>Principle</th>
    <th>Batch</th>
    <th>Expired</th>
    <th>Gudang</th>
    <th>Rak</th>
    <th>Stok Awal</th>
    <th>Masuk</th>
    <th>Keluar</th>
    <th>Adj Masuk</th>
    <th>Adj Keluar</th>
    <th>Stok Akhir</th>
    </tr>
</thead>
<tbody>
    <?php 
        
        $sel = "select t_stok_id,b.nama_barang,c.principle_desc,a.batch,a.expired,gudang_desc,rak_desc,qty_masuk,qty_keluar
        ,qty_adj_masuk,qty_adj_keluar,last_stok,stok_awal
        from t_stok a
        inner join m_barang b on a.m_barang_id = b.m_barang_id
        inner join m_principle c on c.m_principle_id = b.m_principle_id
        inner join m_rak d on d.m_rak_id = a.m_rak_id
        inner join m_gudang e on e.m_gudang_id = d.m_gudang_id $where
        order by a.m_barang_id";
        $result = mysqli_query($con,$sel);
        $i = 1;
        while($res = mysqli_fetch_array($result)){
    ?>
    <tr>
    <td><?php echo $i; ?></td>
    <td><?php echo $res['nama_barang']; ?></td>
    <td><?php echo $res['principle_desc']; ?></td>
    <td><?php echo $res['batch']; ?></td>
    <td><?php echo $res['expired']; ?></td>
    <td><?php echo $res['gudang_desc']; ?></td>
    <td><?php echo $res['rak_desc']; ?></td>
    <td><?php echo $res['stok_awal']; ?></td>
    <td><?php echo $res['qty_masuk']; ?></td>
    <td><?php echo $res['qty_keluar']; ?></td>
    <td><?php echo $res['qty_adj_masuk']; ?></td>
    <td><?php echo $res['qty_adj_keluar']; ?></td>
    <td><?php echo $res['last_stok']; ?></td>
    </tr>
    <?php
        $i =  $i + 1;
        }
        
    ?>
</tbody>
</table>
