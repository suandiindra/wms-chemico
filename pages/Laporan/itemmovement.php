<?php
    $gudang = "--Pilih Gudang--";
    $gudang_id = "";
    $rak = "--Pilih Rak--";
    $rak_id = "";
    $batch = "--Pilih Batch--";
    $item = "--Pilih Item--";
    $item_id = "";
    $principle = "--Pilih Principle--";
    $principle_id = "";
    $where = " where 1=1 ";
    
    if(isset($_POST['lihat'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $filter_gudang = "";
            $filter_rak = "";
            $filter_batch ="";
            $filter_item = "";
            $filter_principle = "";
            // if($_POST['gudang']){
            //     $filter_gudang = " and d.m_kategori_id = '".$_POST['gudang']."'";
            // }
            // if($_POST['rak']){
            //     $filter_rak = " and a.m_rak_id = '".$_POST['rak']."'";
            // }
            // if($_POST['batch']){
            //     $filter_batch = " and a.batch = '".$_POST['batch']."'";
            // }
            if($_POST['item']){
                $filter_item = " and a.m_barang_id = '".$_POST['item']."'";
            }
            if($_POST['principle']){
                $filter_principle = " and b.m_principle_id = '".$_POST['principle']."' ";
            }
            
            
            $where = $where.$filter_gudang.$filter_rak.$filter_batch.$filter_item.$filter_principle;
    
            // echo $where;
        }
    }
    
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Lap Material Item</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Item</li>
    </ol>
    </div>
    <div>
        <div class="form-row col-md-12">
        <form action="" method="POST" class="form-row col-md-12">
                    <?php
                        $rand=rand();
                        $_SESSION['rand']=$rand;
                    ?>  
                    <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                    
                    <div class="col col-md-2">
                        <Select name="item" id="item" class="kodebrg">
                            <option value="">--Plih Item--</option>
                            <?php
                                $sel = "select * from m_barang ";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['m_barang_id'];?>"><?php echo $dtg['nama_barang'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-2">
                        <Select name="principle" id="principle" class="kodebrg">
                            <option value="">--Plih Principle--</option>
                            <?php
                                $sel = "select * from m_principle";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['m_principle_id'];?>"><?php echo $dtg['principle_desc'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-1">
                        <button class="btn btn-primary" name="lihat">Lihat</button>
                    </div>
                    <!-- <div class="col col-md-1">
                        <button class="btn btn-danger float-right" name="export">Export Data</button>
                    </div> -->
               <!-- </div> -->
        </form>
        <div class="col col-md-1">
          <a href="./pages/Laporan/export_itemmovement.php?query=<?php echo $where?>"><button class="btn btn-danger float-left" style="margin-top:10px" name="export">Export Data</button></a>
        </div>
        </div>
    </div>
    <div class="card-body">
        * Demi kecepatan load data untuk melihat detail transaksi silahkan Export Data*
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>#</th>
                        <th>Tanggal</th>
                        <th>Principle</th>
                        <th>Batch</th>
                        <th>Jenis</th>
                        <th>References</th>
                        <th>Source/Dest</th>
                        <th style="text-align:center">Masuk</th>
                        <th style="text-align:center">Keluar</th>
                        <th style="text-align:center">Stok</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          
                          $sel = "select b.m_barang_id, b.nama_barang,c.principle_desc,sum(qty_masuk)qty_masuk,sum(qty_keluar)qty_keluar
                          ,sum(qty_adj_masuk)qty_adj_masuk,sum(qty_adj_keluar)qty_adj_keluar,sum(last_stok)last_stok,sum(stok_awal)stok_awal
                          from t_stok a
                          inner join m_barang b on a.m_barang_id = b.m_barang_id
                          inner join m_principle c on c.m_principle_id = b.m_principle_id
                          inner join m_rak d on d.m_rak_id = a.m_rak_id
                          inner join m_gudang e on e.m_gudang_id = d.m_gudang_id $where
                          group by b.m_barang_id, b.nama_barang,c.principle_desc
                          order by a.m_barang_id";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                              $x =4
                      ?>
                      <tr style="background-color:#F0F0D0; color:black">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['nama_barang']; ?></br><?php echo "[".$res['principle_desc']."]"; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td style="text-align:center"><?php echo number_format($res['stok_awal'],0); ?></td>
                      </tr>
                      <?php
                        $stok_awal = $res['stok_awal'];
                        $m_barang_id = $res['m_barang_id'];
                        $qr = "select a.visual_check_no as id,tipe_gr as ket,'IN' as jenis,a.tgl_gr as periode,c.nama_barang,principle_desc
                        ,batch,rak_desc,b.qty_gr  as jml ,principle_desc tujuan,'background-color:#7FFFD4; color:black' as color,a.penerima as usercreated from t_gr a
                        inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
                        inner join m_barang c on c.m_barang_id = b.m_barang_id 
                        inner join m_principle d on d.m_principle_id = c.m_principle_id
                        inner join m_rak e on e.m_rak_id = b.m_rak_id
                        where b.m_barang_id = '$m_barang_id' 
                        union
                        select a.nomor_do as id,tipe_do as ket,'OUT' as jenis,a.tgl_do,c.nama_barang,principle_desc
                        ,batch,rak_desc,b.qty  as jml,f.customer_desc tujuan,'background-color:#DC143C; color:white' as color,a.usercreated as usercreated from t_do a
                        inner join t_do_detail b on a.t_do_id = b.t_do_id
                        inner join m_barang c on c.m_barang_id = b.m_barang_id 
                        inner join m_principle d on d.m_principle_id = c.m_principle_id
                        inner join m_rak e on e.m_rak_id = b.m_rak_id
                        left join m_customer f on f.m_customer_id = a.m_customer_id
                        where b.m_barang_id = '$m_barang_id' 
                        union
                        select a.t_mutasi_id as id,'Transfer Keluar' as ket,'OUT' as jenis,a.tgl_mutasi as periode,c.nama_barang,principle_desc
                        ,batch,e.rak_desc,b.jumlah  as jml,f.rak_desc tujuan,'background-color:#B22222; color:white' as color,a.usercreated as usercreated  from t_mutasi a
                        inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
                        inner join m_barang c on c.m_barang_id = b.m_barang_id 
                        inner join m_principle d on d.m_principle_id = c.m_principle_id
                        inner join m_rak e on e.m_rak_id = b.m_rak_id_source
                        inner join m_rak f on f.m_rak_id = b.m_rak_id_dest
                        where b.m_barang_id = '$m_barang_id' 
                        union
                        select a.t_mutasi_id as id,'Transfer Masuk' as ket,'IN' as jenis,a.tgl_mutasi as periode,c.nama_barang,principle_desc
                        ,batch,e.rak_desc,b.jumlah  as jml,f.rak_desc tujuan,'background-color:#228B22; color:white' as color,a.usercreated as usercreated  from t_mutasi a
                        inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
                        inner join m_barang c on c.m_barang_id = b.m_barang_id 
                        inner join m_principle d on d.m_principle_id = c.m_principle_id
                        inner join m_rak e on e.m_rak_id = b.m_rak_id_dest
                        inner join m_rak f on f.m_rak_id = b.m_rak_id_source
                        where b.m_barang_id = '$m_barang_id' 
                        union
                        select a.t_adjustment_id as id,'Adjustment Masuk' as ket,'IN' as jenis,a.tgl_adjustment as periode,c.nama_barang
                        ,principle_desc ,batch,rak_desc,b.qty as jml,'' tujuan,'background-color:#20B2AA; color:white' as color,a.usercreated as usercreated from t_adjustment a 
                        inner join t_adjustment_detail b on a.t_adjustment_id = b.t_adjustment_id and b.eksekusi = 'PLUS'
                        inner join m_barang c on c.m_barang_id = b.m_barang_id 
                        inner join m_principle d on d.m_principle_id = c.m_principle_id 
                        inner join m_rak e on e.m_rak_id = b.m_rak_id
                        where b.m_barang_id = '$m_barang_id' 
                        union
                        select a.t_adjustment_id as id,'Adjustment Keluar' as ket,'OUT' as jenis,a.tgl_adjustment as periode,c.nama_barang
                        ,principle_desc ,batch,rak_desc,b.qty as jml,'' tujuan,'background-color:#FF4500; color:white' as color,a.usercreated as usercreated from t_adjustment a 
                        inner join t_adjustment_detail b on a.t_adjustment_id = b.t_adjustment_id and b.eksekusi = 'MINUS'
                        inner join m_barang c on c.m_barang_id = b.m_barang_id 
                        inner join m_principle d on d.m_principle_id = c.m_principle_id 
                        inner join m_rak e on e.m_rak_id = b.m_rak_id
                        where b.m_barang_id = '$m_barang_id'
                        union
                        select a.t_retur_id as id,'Barang Retur' as ket,'OUT' as jenis,a.tgl_retur,c.nama_barang,principle_desc
                        ,batch,rak_desc,b.qty  as jml,f.vendor_desc tujuan,'background-color:#FF6347; color:white' as color,a.usercreated as usercreated   from t_retur a
                        inner join t_retur_detail b on a.t_retur_id = b.t_retur_id
                        inner join m_barang c on c.m_barang_id = b.m_barang_id 
                        inner join m_principle d on d.m_principle_id = c.m_principle_id
                        inner join m_rak e on e.m_rak_id = b.m_rak_id
                        left join m_vendor f on f.m_vendor_id = a.m_vendor_id
                        where b.m_barang_id = '$m_barang_id' ";

                        $result2 = mysqli_query($con,$qr);
                        while($det = mysqli_fetch_array($result2)){
                        $color = "";
                            ?>
                            <tr style="<?php echo $color; ?>">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $det['periode']; ?></td>
                                <td><?php echo $det['principle_desc']; ?></td>
                                <td><?php echo $det['batch']; ?></td>
                                <td><?php echo $det['ket']; ?></td>
                                <td><?php echo $det['id']; ?></td>
                                <td><?php echo $det['tujuan']; ?></td>
                                <td style="text-align:center"><?php 
                                    if($det['jenis'] == 'IN'){
                                        echo $det['jml'];
                                        $stok_awal = format($stok_awal + $det['jml']);
                                    } 
                                ?></td>
                                <td style="text-align:center">
                                    <?php
                                        if($det['jenis'] == 'OUT'){
                                            echo $det['jml'];
                                            $stok_awal = format($stok_awal - $det['jml']);
                                        }
                                    ?>
                                </td>
                                <td style="text-align:center">
                                    <?php
                                        echo number_format($stok_awal,0);
                                    ?>
                                </td>
                            </tr>
                            <?php
                            }
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
        </div>
    </div>
</div>
<?php
    if(isset($_POST['export'])){
        if($_POST['randcheck']==$_SESSION['rand']){
        $filter_gudang = "";
            $filter_rak = "";
            $filter_batch ="";
            $filter_item = "";
            $filter_principle = "";
            if($_POST['gudang']){
                $filter_gudang = " and e.m_gudang_id = '".$_POST['gudang']."'";
            }
            if($_POST['rak']){
                $filter_rak = " and a.m_rak_id = '".$_POST['rak']."'";
            }
            if($_POST['batch']){
                $filter_batch = " and a.batch = '".$_POST['batch']."'";
            }
            if($_POST['item']){
                $filter_item = " and a.m_barang_id = '".$_POST['item']."'";
            }
            if($_POST['principle']){
                $filter_principle = " and b.m_principle_id = '".$_POST['principle']."' ";
            }
            
            
            $where = $where.$filter_gudang.$filter_rak.$filter_batch.$filter_item.$filter_principle;
            echo "<script>window.location='./pages/Laporan/export_itembylocation.php?query=$where'</script>";
        }
    }
?>
<script>
 $(".kodebrg").chosen();
    function confirmation(delName){
    var del=confirm("Yakin Ingin menghapus PO ini..??");
    if (del==true){
        window.location.href="./pages/Transaksi PO/action.php?act=del&id="+delName;
    }
    return del;
}
</script>