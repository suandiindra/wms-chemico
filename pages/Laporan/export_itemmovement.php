<?php
    session_start();
    $m_user_id = $_SESSION['user_id'];
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $where = $_GET['query'];
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Lap Material Item.xls");
    $mydate=getdate(date("U"));
    $tgl = "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year]";
?>
<table>
    <tr>
        <td><h3><b>PT. Chemico Surabaya</b></h3></td>
    </tr>
    <tr>
        <td><b>Lap Material Item</b></td>
    </tr>
    <tr>
        <td><b><?php echo $tgl; ?></b></td>
    </tr>
</table>
<table border=1>
    <thead class="thead-light">
        <tr>
        <th>#</th>
        <th>Tanggal</th>
        <th>Principle</th>
        <th>Batch</th>
        <th>Jenis</th>
        <th>References</th>
        <th>Source/Dest</th>
        <th style="text-align:center">Masuk</th>
        <th style="text-align:center">Keluar</th>
        <th style="text-align:center">Stok</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            
            $sel = "select b.m_barang_id, b.nama_barang,c.principle_desc,sum(qty_masuk)qty_masuk,sum(qty_keluar)qty_keluar
            ,sum(qty_adj_masuk)qty_adj_masuk,sum(qty_adj_keluar)qty_adj_keluar,sum(last_stok)last_stok,sum(stok_awal)stok_awal
            from t_stok a
            inner join m_barang b on a.m_barang_id = b.m_barang_id
            inner join m_principle c on c.m_principle_id = b.m_principle_id
            inner join m_rak d on d.m_rak_id = a.m_rak_id
            inner join m_gudang e on e.m_gudang_id = d.m_gudang_id $where
            group by b.m_barang_id, b.nama_barang,c.principle_desc
            order by a.m_barang_id";
            $result = mysqli_query($con,$sel);
            $i = 1;
            while($res = mysqli_fetch_array($result)){
                $x =4
        ?>
        <tr style="background-color:#F0F0D0; color:black">
        <td><?php echo $i; ?></td>
        <td colspan="8"><?php echo $res['nama_barang']; ?></br><?php echo "  [".$res['principle_desc']."]"; ?></td>
        <td style="text-align:center"><?php echo number_format($res['stok_awal'],2); ?></td>
        </tr>
        <?php
        $stok_awal = $res['stok_awal'];
        $m_barang_id = $res['m_barang_id'];
        $qr = "select a.visual_check_no as id,tipe_gr as ket,'IN' as jenis,a.tgl_gr as periode,c.nama_barang,principle_desc
        ,batch,rak_desc,b.qty_gr  as jml ,principle_desc tujuan,'background-color:#7FFFD4; color:black' as color,a.penerima as usercreated from t_gr a
        inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        where b.m_barang_id = '$m_barang_id' 
        union
        select a.nomor_do as id,tipe_do as ket,'OUT' as jenis,a.tgl_do,c.nama_barang,principle_desc
        ,batch,rak_desc,b.qty  as jml,f.customer_desc tujuan,'background-color:#DC143C; color:white' as color,a.usercreated as usercreated from t_do a
        inner join t_do_detail b on a.t_do_id = b.t_do_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        left join m_customer f on f.m_customer_id = a.m_customer_id
        where b.m_barang_id = '$m_barang_id' 
        union
        select a.t_mutasi_id as id,'Transfer Keluar' as ket,'OUT' as jenis,a.tgl_mutasi as periode,c.nama_barang,principle_desc
        ,batch,e.rak_desc,b.jumlah  as jml,f.rak_desc tujuan,'background-color:#B22222; color:white' as color,a.usercreated as usercreated  from t_mutasi a
        inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id_source
        inner join m_rak f on f.m_rak_id = b.m_rak_id_dest
        where b.m_barang_id = '$m_barang_id' 
        union
        select a.t_mutasi_id as id,'Transfer Masuk' as ket,'IN' as jenis,a.tgl_mutasi as periode,c.nama_barang,principle_desc
        ,batch,e.rak_desc,b.jumlah  as jml,f.rak_desc tujuan,'background-color:#228B22; color:white' as color,a.usercreated as usercreated  from t_mutasi a
        inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id_dest
        inner join m_rak f on f.m_rak_id = b.m_rak_id_source
        where b.m_barang_id = '$m_barang_id' 
        union
        select a.t_adjustment_id as id,'Adjustment Masuk' as ket,'IN' as jenis,a.tgl_adjustment as periode,c.nama_barang
        ,principle_desc ,batch,e.rak_desc,b.qty as jml,e.rak_desc tujuan,'background-color:#20B2AA; color:white' as color,a.usercreated as usercreated from t_adjustment a 
        inner join t_adjustment_detail b on a.t_adjustment_id = b.t_adjustment_id and b.eksekusi = 'PLUS'
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id 
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        where b.m_barang_id = '$m_barang_id' 
        union
        select a.t_adjustment_id as id,'Adjustment Keluar' as ket,'OUT' as jenis,a.tgl_adjustment as periode,c.nama_barang
        ,principle_desc ,batch,e.rak_desc,b.qty as jml,e.rak_desc tujuan,'background-color:#FF4500; color:white' as color,a.usercreated as usercreated from t_adjustment a 
        inner join t_adjustment_detail b on a.t_adjustment_id = b.t_adjustment_id and b.eksekusi = 'MINUS'
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id 
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        where b.m_barang_id = '$m_barang_id'
        union
        select a.t_retur_id as id,'Barang Retur' as ket,'OUT' as jenis,a.tgl_retur,c.nama_barang,principle_desc
        ,batch,rak_desc,b.qty  as jml,f.vendor_desc tujuan,'background-color:#FF6347; color:white' as color,a.usercreated as usercreated   from t_retur a
        inner join t_retur_detail b on a.t_retur_id = b.t_retur_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        left join m_vendor f on f.m_vendor_id = a.m_vendor_id
        where b.m_barang_id = '$m_barang_id' ";

        $result2 = mysqli_query($con,$qr);
        $jml_in = 0;
        $jml_out = 0;
            while($det = mysqli_fetch_array($result2)){
            $color = "";
        ?>
                <tr style="<?php echo $color; ?>">
                    <td></td>
                    <td><?php echo $det['periode']; ?></td>
                    <td><?php echo $det['principle_desc']; ?></td>
                    <td><?php echo $det['batch']; ?></td>
                    <td><?php echo $det['ket']; ?></td>
                    <td><?php echo $det['id']; ?></td>
                    <td><?php echo $det['tujuan']; ?></td>
                    <td style="text-align:center"><?php 
                        if($det['jenis'] == 'IN'){
                            echo format($det['jml']);
                            $jml_in = $jml_in + $det['jml'];
                            $stok_awal = $stok_awal + $det['jml'];
                        } 
                    ?></td>
                    <td style="text-align:center">
                        <?php
                            if($det['jenis'] == 'OUT'){
                                echo format($det['jml']);
                                $jml_out = $jml_out + $det['jml'];
                                $stok_awal = $stok_awal - $det['jml'];
                            }
                        ?>
                    </td>
                    <td style="text-align:center">
                        <?php
                            echo number_format($stok_awal,2);
                        ?>
                    </td>
                </tr>
        <?php
            }
        ?>
        <tr>
            <td></td>
            <td colspan=6 style="text-align:center"><b>Total</b></td>
            <td style="text-align:center"><b><?php echo $jml_in; ?></b></td>
            <td style="text-align:center"><b><?php echo $jml_out; ?></b></td>
            <td style="text-align:center"><b><?php echo format($stok_awal); ?></b></td>
        </tr>
        <?php
            $i =  $i + 1;
            }
        ?>
    </tbody>
    </table>