<?php
    session_start();
    $m_user_id = $_SESSION['user_id'];
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $where = $_GET['query'];
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Lap Penukaran DO.xls");
    $mydate=getdate(date("U"));
    $tgl = "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year]";
    
?>
<table>
    <tr>
        <td><h3><b>PT. Chemico Surabaya</b></h3></td>
    </tr>
    <tr>
        <td><b>Lap Penukaran DO</b></td>
    </tr>
    <tr>
        <td><b><?php echo $tgl; ?></b></td>
    </tr>
</table>

<table border=1>
<thead class="thead-light">
    <tr>
    <th>No.</th>
    <th>Nomor DO</th>
    <th>Tgl DO</th>
    <th>Customer</th>
    <th>Tipe DO</th>
    <th>Nama Barang</th>
    <th>Principle</th>
    <th>Batch</th>
    <th>Rak</th>
    <th>QTY</th>
    <th>Dibuat Oleh</th>
    </tr>
</thead>
<tbody>
    <?php 
        
        $sel = "select a.t_do_id,a.nomor_do,tgl_do,e.customer_desc,tipe_do
        ,c.nama_barang,g.principle_desc,b.Batch, h.rak_desc
        ,reference_key,b.qty,a.usercreated 
        ,a.catatan,f.t_gr_id from t_do a
        inner join t_do_detail b on a.t_do_id = b.t_do_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id
        inner join t_stok d on d.m_barang_id = b.m_barang_id and d.batch = d.batch and b.m_rak_id = d.m_rak_id
        inner join m_customer e on e.m_customer_id = a.m_customer_id 
        left join t_gr f on f.visual_check_no = a.catatan 
        inner join m_principle g on g.m_principle_id = c.m_principle_id
        inner join m_rak h on h.m_rak_id = b.m_rak_id $where ";
        $result = mysqli_query($con,$sel);
        $i = 1;
        while($res = mysqli_fetch_array($result)){
    ?>
    <tr>
    <td><?php echo $i; ?></td>
    <td><?php echo $res['nomor_do']; ?></td>
    <td><?php echo $res['tgl_do']; ?></td>
    <td><?php echo $res['customer_desc']; ?></td>
    <td><?php echo $res['tipe_do']; ?></td>
    <td><?php echo $res['nama_barang']; ?></td>
    <td><?php echo $res['principle_desc']; ?></td>
    <td><?php echo $res['batch']; ?></td>
    <td><?php echo $res['rak_desc']; ?></td>
    <td><?php echo format($res['qty']); ?></td>
    <td><?php echo $res['usercreated']; ?></td>
    </tr>
    <?php
        $i =  $i + 1;
    }
    ?>
</tbody>
</table>
