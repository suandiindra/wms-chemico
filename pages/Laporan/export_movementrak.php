<?php
    session_start();
    $m_user_id = $_SESSION['user_id'];
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $where = $_GET['query'];
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Lap Transfer Rak.xls");
   
    $mydate=getdate(date("U"));
    $tgl = "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year]";
?>
<table>
    <tr>
        <td><h3><b>PT. Chemico Surabaya</b></h3></td>
    </tr>
    <tr>
        <td><b>Lap Transfer Rak</b></td>
    </tr>
    <tr>
        <td><b><?php echo $tgl; ?></b></td>
    </tr>
</table>
<table border=1>
<thead class="thead-light">
    <tr>
        <th>No.</th>
        <th>Tgl Mutasi</th>
        <th>Nama Barang</th>
        <th>Batch</th>
        <th>Principle</th>
        <th>Rak Awal</th>
        <th>Stok Awal</th>
        <th>Rak Tujuan</th>
        <th>Sisa</th>
        <th>QTY</th>
        <th>PIC</th>
    </tr>
</thead>
<tbody>
    <?php                
        $sel = "select x.*,y.rak_desc as destination from
        (
            select tgl_mutasi,b.m_barang_id,nama_barang,principle_desc,b.batch 
            ,gudang_desc,b.m_rak_id_source,m_rak_id_dest,d.rak_desc,b.jumlah,a.usercreated,stok_awal,
            stok_awal - b.jumlah as sisa,a.createdate
            from t_mutasi a
            inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
            inner join m_barang c on  c.m_barang_id = b.m_barang_id
            inner join m_principle e on e.m_principle_id = c.m_principle_id
            inner join m_rak d on d.m_rak_id = b.m_rak_id_source
            inner join m_gudang f on f.m_gudang_id = d.m_gudang_id
            $where
        )x join m_rak y on x.m_rak_id_dest = y.m_rak_id order by createdate desc ";
        $result = mysqli_query($con,$sel);
        $i = 1;
        while($res = mysqli_fetch_array($result)){
    ?>
    <tr>
    <td><?php echo $i; ?></td>
    <td><?php echo $res['tgl_mutasi']; ?></td>
    <td><?php echo $res['nama_barang']; ?></td>
    <td><?php echo $res['batch']; ?></td>
    <td><?php echo $res['principle_desc']; ?></td>
    <td><?php echo $res['rak_desc']; ?></td>
    <td><?php echo format($res['stok_awal']); ?></td>
    <td><?php echo $res['destination']; ?></td>
    <td><?php echo format($res['sisa']); ?></td>
    <td><?php echo format($res['jumlah']); ?></td>
    <td><?php echo $res['usercreated']; ?></td>
    </tr>
    <?php
        $i =  $i + 1;
    }
    ?>
</tbody>
</table>
