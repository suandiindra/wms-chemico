<?php
    $gudang = "--Pilih Gudang--";
    $gudang_id = "";
    $rak = "--Pilih Rak--";
    $rak_id = "";
    $batch = "--Pilih Batch--";
    $item = "--Pilih Item--";
    $item_id = "";
    $principle = "--Pilih Principle--";
    $principle_id = "";
    $where = " where 1=1 ";
    
    if(isset($_POST['lihat'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $filter_gudang = "";
            $filter_rak = "";
            $filter_batch ="";
            $filter_item = "";
            $filter_principle = "";
            if($_POST['gudang']){
                $filter_gudang = " and d.m_kategori_id = '".$_POST['gudang']."'";
            }
            if($_POST['rak']){
                $filter_rak = " and a.m_rak_id = '".$_POST['rak']."'";
            }
            if($_POST['batch']){
                $filter_batch = " and a.batch = '".$_POST['batch']."'";
            }
            if($_POST['item']){
                $filter_item = " and a.m_barang_id = '".$_POST['item']."'";
            }
            if($_POST['principle']){
                $filter_principle = " and b.m_principle_id = '".$_POST['principle']."' ";
            }
            
            
            $where = $where.$filter_gudang.$filter_rak.$filter_batch.$filter_item.$filter_principle;
    
            // echo $where;
        }
    }
    
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Lap Item by Location</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Stok</li>
    </ol>
    </div>
    <div>
        <div class="form-row col-md-12">
        <form action="" method="POST" class="form-row col-md-12">
                    <?php
                        $rand=rand();
                        $_SESSION['rand']=$rand;
                    ?>  
                    <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                    <div class="col col-md-2">
                        <Select name="gudang" class="form-control">
                            <option value="" >--Plih Kategori--</option>
                            <?php
                                $sel = "select * from m_kategori";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['m_kategori_id'];?>"><?php echo $dtg['kategory_desc'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-2">
                        <Select name="rak" id="rak_f" class="form-control">
                            <option value="">--Plih rak--</option>
                            <?php
                                $sel = "select a.m_rak_id,a.rak_desc from m_rak a
                                inner join m_gudang b on a.m_gudang_id = b.m_gudang_id
                                group by a.m_rak_id,a.rak_desc";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['m_rak_id'];?>"><?php echo $dtg['rak_desc'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-2">
                        <Select name="batch" id="batch" class="form-control">
                            <option value="">--Plih Batch--</option>
                            <?php
                                $sel = "select batch from t_stok group by batch";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['batch'];?>"><?php echo $dtg['batch'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-2">
                        <Select name="item" id="item" class="kodebrg">
                            <option value="">--Plih Item--</option>
                            <?php
                                $sel = "select * from m_barang ";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['m_barang_id'];?>"><?php echo $dtg['nama_barang'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-2">
                        <Select name="principle" id="principle" class="kodebrg">
                            <option value="">--Plih Principle--</option>
                            <?php
                                $sel = "select * from m_principle";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['m_principle_id'];?>"><?php echo $dtg['principle_desc'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-1">
                        <button class="btn btn-primary" name="lihat">Lihat</button>
                    </div>
                    <!-- <div class="col col-md-1">
                        <button class="btn btn-danger float-right" name="export">Export Data</button>
                    </div> -->
               <!-- </div> -->
        </form>
        <div class="col col-md-1">
          <a href="./pages/Laporan/export_itembylocation.php?query=<?php echo $where?>"><button class="btn btn-danger float-left" style="margin-top:10px" name="export">Export Data</button></a>
        </div>
        </div>
    </div>
    <div class="card-body">
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No.</th>
                        <th>Nama Barang</th>
                        <th>Principle</th>
                        <th>Batch</th>
                        <th>Expired</th>
                        <th>Gudang</th>
                        <th>Rak</th>
                        <th>Stok Awal</th>
                        <th>Masuk</th>
                        <th>Keluar</th>
                        <th>Adj Masuk</th>
                        <th>Adj Keluar</th>
                        <th>Stok Akhir</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          
                          $sel = "select t_stok_id,b.nama_barang,c.principle_desc,a.batch,a.expired,gudang_desc,rak_desc,qty_masuk,qty_keluar
                          ,qty_adj_masuk,qty_adj_keluar,last_stok,stok_awal
                          from t_stok a
                          inner join m_barang b on a.m_barang_id = b.m_barang_id
                          inner join m_principle c on c.m_principle_id = b.m_principle_id
                          inner join m_rak d on d.m_rak_id = a.m_rak_id
                          inner join m_gudang e on e.m_gudang_id = d.m_gudang_id $where
                          order by a.m_barang_id";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['nama_barang']; ?></td>
                        <td><?php echo $res['principle_desc']; ?></td>
                        <td><?php echo $res['batch']; ?></td>
                        <td><?php echo $res['expired']; ?></td>
                        <td><?php echo $res['gudang_desc']; ?></td>
                        <td><?php echo $res['rak_desc']; ?></td>
                        <td><?php echo format($res['stok_awal']); ?></td>
                        <td><?php echo format($res['qty_masuk']); ?></td>
                        <td><?php echo format($res['qty_keluar']); ?></td>
                        <td><?php echo format($res['qty_adj_masuk']); ?></td>
                        <td><?php echo format($res['qty_adj_keluar']); ?></td>
                        <td><?php echo format($res['last_stok']); ?></td>
                        <td style="text-align:center">
                           <a href="./?route=stokmap&id=<?php echo $res['t_stok_id']; ?>"><button class="btn btn-success">Lihat</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
        </div>
    </div>
</div>
<?php
    if(isset($_POST['export'])){
        if($_POST['randcheck']==$_SESSION['rand']){
        $filter_gudang = "";
            $filter_rak = "";
            $filter_batch ="";
            $filter_item = "";
            $filter_principle = "";
            if($_POST['gudang']){
                $filter_gudang = " and e.m_gudang_id = '".$_POST['gudang']."'";
            }
            if($_POST['rak']){
                $filter_rak = " and a.m_rak_id = '".$_POST['rak']."'";
            }
            if($_POST['batch']){
                $filter_batch = " and a.batch = '".$_POST['batch']."'";
            }
            if($_POST['item']){
                $filter_item = " and a.m_barang_id = '".$_POST['item']."'";
            }
            if($_POST['principle']){
                $filter_principle = " and b.m_principle_id = '".$_POST['principle']."' ";
            }
            
            
            $where = $where.$filter_gudang.$filter_rak.$filter_batch.$filter_item.$filter_principle;
            echo "<script>window.location='./pages/Laporan/export_itembylocation.php?query=$where'</script>";
        }
    }
?>
<script>
 $(".kodebrg").chosen();
    function confirmation(delName){
    var del=confirm("Yakin Ingin menghapus PO ini..??");
    if (del==true){
        window.location.href="./pages/Transaksi PO/action.php?act=del&id="+delName;
    }
    return del;
}
</script>