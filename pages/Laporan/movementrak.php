<?php
    $where = " where 1=1 ";
    

    if(isset($_POST['lihat'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $filter_tgl = "";
            $filter_item = "";
            $filter_source_rak = "";
            $filter_tujuan_rak = "";
            if($_POST['date1']){
                $filter_tgl = " and tgl_mutasi between '".$_POST['date1']."' and '".$_POST['date2']."'";
            }
            if($_POST['item']){
                $filter_item = " and b.m_barang_id = '".$_POST['item']."' ";
            }


            if($_POST['rak_awal']){
                $filter_source_rak = " and m_rak_id_source = '".$_POST['rak_awal']."'";
            }


            if($_POST['rak_tujuan']){
                $filter_tujuan_rak = " and m_rak_id_dest = '".$_POST['rak_tujuan']."'";
            }
            $where = $where.$filter_item.$filter_tgl.$filter_source_rak.$filter_tujuan_rak;
        }
    }
    
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Lap Transfer Rak</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Laporan</li>
    </ol>
    </div>
    <div>
        <div class="form-row col-md-12">
        <form action="" method="POST" class="form-row col-md-12">
        <?php
            $rand=rand();
            $_SESSION['rand']=$rand;
        ?>  
        <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                    <div class="col col-md-2">
                    <label>Periode Mutasi</label>
                       <input type="date" class="form-control" name="date1"/>
                    </div>
                    <div class="col col-md-2">
                    <label>Periode Mutasi</label>
                        <input type="date" class="form-control" name="date2"/>  
                    </div>
                    
                    <div class="col col-md-2">
                    <label>Item</label>
                        <Select name="item" id="item" class="kodebrg">
                            <option value="">--Plih Item--</option>
                            <?php
                                $sel = "select * from m_barang ";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['m_barang_id'];?>"><?php echo $dtg['nama_barang'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-2">
                    <label>Rak Awal</label>
                        <Select name="rak_awal" id="rak_awal" class="form-control">
                            <option value="">--Rak Awal--</option>
                            <?php
                                $sel = "select * from m_rak";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['m_rak_id'];?>"><?php echo $dtg['rak_desc'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-2">
                    <label>Rak Tujuan</label>
                        <Select name="rak_tujuan" id="rak_tujuan" class="form-control">
                            <option value="">--Rak Tujuan--</option>
                            <?php
                                $sel = "select * from m_rak";
                                $res = mysqli_query($con,$sel);
                                while($dtg = mysqli_fetch_array($res)){
                            ?>
                                 <option value="<?php echo $dtg['m_rak_id'];?>"><?php echo $dtg['rak_desc'];?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-2">
                        <button class="btn btn-primary" style="margin-top:30px" name="lihat">Lihat</button>
                    </div>
               <!-- </div> -->
        </form>
            <div class="col col-md-2">
            <a href="./pages/Laporan/export_movementrak.php?query=<?php echo $where?>"><button class="btn btn-danger" style="margin-top:10px" name="lihat">Export Data</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No.</th>
                        <th>Tgl Mutasi</th>
                        <th>Nama Barang</th>
                        <th>Batch</th>
                        <th>Principle</th>
                        <th>Rak Awal</th>
                        <th>Stok Awal</th>
                        <th>Rak Tujuan</th>
                        <th>Sisa</th>
                        <th>QTY</th>
                        <th>PIC</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          
                          $sel = "select x.*,y.rak_desc as destination from
                          (
                              select tgl_mutasi,b.m_barang_id,nama_barang,principle_desc,b.batch 
                              ,gudang_desc,b.m_rak_id_source,m_rak_id_dest,d.rak_desc,b.jumlah,a.usercreated,stok_awal,
                              stok_awal - b.jumlah as sisa,a.createdate
                              from t_mutasi a
                              inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
                              inner join m_barang c on  c.m_barang_id = b.m_barang_id
                              inner join m_principle e on e.m_principle_id = c.m_principle_id
                              inner join m_rak d on d.m_rak_id = b.m_rak_id_source
                              inner join m_gudang f on f.m_gudang_id = d.m_gudang_id
                              $where
                          )x join m_rak y on x.m_rak_id_dest = y.m_rak_id order by createdate desc ";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['tgl_mutasi']; ?></td>
                        <td><?php echo $res['nama_barang']; ?></td>
                        <td><?php echo $res['batch']; ?></td>
                        <td><?php echo $res['principle_desc']; ?></td>
                        <td><?php echo $res['rak_desc']; ?></td>
                        <td><?php echo format($res['stok_awal']); ?></td>
                        <td><?php echo $res['destination']; ?></td>
                        <td><?php echo format($res['sisa']); ?></td>
                        <td><?php echo format($res['jumlah']); ?></td>
                        <td><?php echo $res['usercreated']; ?></td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
        </div>
    </div>
</div>
<?php
    if(isset($_POST['export2'])){
        // if($_POST['randcheck']==$_SESSION['rand']){
        $filter_tgl = "";
        $filter_item = "";
        $filter_source_rak = "";
        $filter_tujuan_rak = "";
        if($_POST['date1']){
            $filter_tgl = " and tgl_mutasi between '".$_POST['date1']."' and '".$_POST['date2']."'";
        }
        if($_POST['item']){
            $filter_item = " and a.m_barang_id = '".$_POST['item']."'";
        }


        if($_POST['rak_awal']){
            $filter_source_rak = " and m_rak_id_source = '".$_POST['rak_awal']."'";
        }


        if($_POST['rak_tujuan']){
            $filter_tujuan_rak = " and m_rak_id_dest = '".$_POST['rak_tujuan']."'";
        }
        $where = $where.$filter_tgl.$filter_source_rak.$filter_tujuan_rak;
        echo "<script>window.location='./pages/Laporan/export_movementrak.php?query=$where'</script>";
        // echo "window.location='./pages/Laporan/export_movementrak.php?query=$where";
        // }
    }
?>
<script>
 $(".kodebrg").chosen();
    function confirmation(delName){
    var del=confirm("Yakin Ingin menghapus PO ini..??");
    if (del==true){
        window.location.href="./pages/Transaksi PO/action.php?act=del&id="+delName;
    }
    return del;
}
</script>