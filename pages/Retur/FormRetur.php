<?php
    $m_user_id  = $_SESSION['user_id'];
?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#kode").change(function(){
            var kode = $("#kode").val();
            // console.log(kode);
            $.ajax({
                url:'pages/Keluar/ajaxData.php',
                method : 'post',
                data : 'kode= ' + kode 
            }).done(function(value){
                console.log(value)
                $('#batch').html(value);
            })
        })
        // $("#kode").on('change',function(e){
        //     var kode = $("#kode").val();
        //     // console.log(kode);
        //     $.ajax({
        //         url:'pages/Keluar/ajaxData.php',
        //         method : 'post',
        //         data : 'kode= ' + kode 
        //     }).done(function(value){
        //         console.log(value)
        //         $('#batch').html(value);
        //     })
        // })

        $("#batch").change(function(){
            var kode = $("#batch").val();
            var barang = document.getElementById("kode").value;
            // console.log(kode);
            $.ajax({
                url:'pages/Keluar/ajaxData.php',
                method : 'post',
                data : 'kode_batch= ' + kode +'&barang='+barang
            }).done(function(vl){
                console.log(vl)
                $('#rak_').html(vl);
                // $('#rak').val(vl);
            })
        })


        $("#partialretur").load("pages/Retur/ajaxLoad.php",{
        
        });


        $("#detailbtn").click(function(){
            var kode = document.getElementById("kode").value;
            var batch = document.getElementById("batch").value;
            var rak_ = document.getElementById("rak_").value;
            var jml  = document.getElementById("jml").value;
            $("#partialretur").load("pages/Retur/ajaxLoad.php",{
                kode : kode,
                batch : batch,
                rak : rak_,
                jml : jml
            })
            // console.log(kode,batch,rak_,jml)
        })
        
    })
</script>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Retur Form</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Input Retur</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
    <form action="./pages/Retur/actionretur.php" method="POST" >
        <div class="row">
            <div class="col-lg-8">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />

                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <input type="text" onkeyup="refered()" required placeholder="Nomor Retur" name="nodo" class="form-control" id="nodo" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <input type="date" onchange="referedtgl()" required  name="tgldo" required class="form-control" id="tgldo" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <select name="customer" onchange="referedtgl()" required class="ven" id="customerdo">
                                <option value="">- Pilih Vendor -</option>
                                <?php
                                    $varquery = "select * from m_vendor";
                                    $res = mysqli_query($con,$varquery);
                                    while($ds = mysqli_fetch_array($res)){
                                ?>
                                    <option value="<?php echo $ds['m_vendor_id'] ?>"><?php echo $ds['vendor_desc'] ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                        </div>
                    </div>
                    <!-- <div class="col">
                        <div class="form-group">
                               <button class="btn btn-success" name="simpando">Proses Retur</button>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        </form>
        <hr>    
        <!-- <form action="./pages/Retur/actionretur.php" method="POST"> -->
        <div class="form-row md-col-12">  
            <div class="col">
                <div class="form-group">
                    <select name="kode" require id="kode" class="form-control" >
                    <option value="">-- Pilih --</option>
                    <?php
                        $s = "select a.m_barang_id,nama_barang from t_stok a
                        inner join m_barang b on a.m_barang_id = b.m_barang_id
                        group by a.m_barang_id,nama_barang;";
                        $res = mysqli_query($con,$s);
                        while($dt = mysqli_fetch_array($res)){
                    ?>
                        <option value="<?php echo $dt['m_barang_id'] ?>"><?php echo $dt['nama_barang']; ?></option>
                    <?php
                        }
                    ?>
                </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                <select name="batch" id="batch" require class="form-control">
                    <option value="">-- Pilih Batch --</option>
                </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                <select name="rak_" id="rak_" require class="form-control">
                    <option value="">-- Pilih Rak --</option>
                </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                        <input type="text" id="jml" require  name="qty" required placeholder="(Kg)" class="form-control" id="exampleInputFirstName" > 
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                     <BUTTon class="btn btn-primary" name="detailbtn" id="detailbtn">Add</BUTTon>
                </div>
            </div>
        </div>
        <!-- </form> -->
        <hr>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Item</th>
            <th>Batch</th>
            <th>Gudang Rak</th>
            <th>QTY</th>
            <th>Satuan</th>
            <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody id="partialretur">
            
        </tbody>
        </table>
        <form action="./pages/Retur/actionretur.php" method="POST">
            <script>
            function refered(){
                var nopo_data = document.getElementById('nodo').value;
                document.getElementById('nomordo').value = nopo_data ;
                
            }
            function referedtgl(){
                var nopo_data = document.getElementById('tgldo').value;
                var customer = document.getElementById('customerdo').value;
                document.getElementById('tanggaldo').value = nopo_data ;
                document.getElementById('custdo').value = customer ;
            }
            </script>
            <input type="hidden" name="nodo" id="nomordo">
            <input type="hidden" name="tgldo" id="tanggaldo">
            <input type="hidden" name="customer" id="custdo">

            <div class="col" style="margin-left:-15px">
                <button class="btn btn-success" name="simpando">Proses Retur</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(".chosenbarang").chosen();
    $(".ven").chosen();
</script>