<?php
    $m_user_id  = $_SESSION['user_id'];
    $id = $_GET['id'];
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Retur Detail</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Input Retur</li>
    </ol>
    </div>
    <?php
        $res = mysqli_query($con,"select * from t_retur a inner join m_vendor b on a.m_vendor_id = b.m_vendor_id 
        where t_retur_id = '$id'");
        $da = mysqli_fetch_array($res);
    ?>
    <div class="col">
        <table>
            <tr>
                <td>Tanggal Retur</td>
                <td style="padding-left:20px;padding-right:20px">:</td>
                <td><?php echo $da['tgl_retur']; ?></td>
            </tr>
            <tr>
                <td>Vendor</td>
                <td style="padding-left:20px;padding-right:20px">:</td>
                <td><?php echo $da['vendor_desc']; ?></td>
            </tr>
            <!-- <tr>
                <td>Catatan</td>
                <td style="padding-left:20px;padding-right:20px">:</td>
                <td><?php echo $da['tgl_retur']; ?></td>
            </tr> -->
        </table>
    </div>
    <hr>
    <div class="card-body">
    
        <!-- <hr> -->
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Item</th>
            <th>Batch</th>
            <th>Gudang Rak</th>
            <th>QTY</th>
            <th>Satuan</th>
            <!-- <th style="text-align:center">Action</th> -->
            </tr>
        </thead>
        <tbody>
            <?php 
                $sel = "select * from t_retur_detail a
                left join t_stok b on a.m_barang_id = b.m_barang_id and a.batch = b.batch and a.m_rak_id = b.m_rak_id
                left join m_barang c on c.m_barang_id = a.m_barang_id
                left join m_rak d on d.m_rak_id = b.m_rak_id
                where a.t_retur_id = '$id'";
                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
            ?>
            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['batch']; ?></td>
            <td><?php echo $res['rak_desc']; ?></td>
            <td><?php echo $res['qty']; ?></td>
            <td><?php echo "Kg" ?></td>
            <!-- <td style="text-align:center">
                <a href="./pages/Retur/actionretur.php?act=del&id=<?php echo $res['t_retur_id']; ?>">
                    <button class="btn btn-warning">Hapus.</button>
                </a>
            </td> -->
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
        
    </div>
</div>
<script>
    $(".chosenbarang").chosen();
</script>