<?php
    $m_user_id  = $_SESSION['user_id'];
    $nomutasi = "";
?>

<?php
    $id = $_GET['id'];
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Adjustment Stok Gudang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Adjustment</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-8">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
            </div>
        </div>
        
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Item</th>
            <th>Batch</th>
            <th>Rak Asal</th>
            <th>QTY (Kg)</th>
            <th>ADJ (+/-)</th>
            <th>Catatan</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $sel = "select b.t_adjustment_detail_id,b.t_adjustment_id,c.m_barang_id,c.nama_barang,b.batch,d.rak_desc,b.qty,eksekusi,b.catatan from t_adjustment a
                inner join t_adjustment_detail b on a.t_adjustment_id = b.t_adjustment_id
                inner join m_barang c on c.m_barang_id = b.m_barang_id
                inner join m_rak d on d.m_rak_id = b.m_rak_id
                where a.t_adjustment_id = '$id'";
                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
            ?>
            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['batch']; ?></td>
            <td><?php echo $res['rak_desc']; ?></td>
            <td><?php echo $res['qty'] ; ?></td>
            <td><?php echo $res['eksekusi'] ?></td>
            <td><?php echo $res['catatan'] ?></td>
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
       
    </div>
    <a href="./?route=adjustment"><Button class="btn btn-warning">Kembali</button></a>
</div>