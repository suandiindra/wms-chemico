<?php
    $where = "";
    if(isset($_POST['lihatdo'])){
      $date1 = $_POST['date1'];
      $date2 = $_POST['date2'];  
      $cust  = $_POST['cust'];
      $where = " where 1=1";
      $where = $where." and tgl_do between '$date1' and '$date2'";
      if($cust){
        $where = $where. " and a.m_customer_id = '$cust'";
      }
    }
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Adjustment Barang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Adjustment</li>
    </ol>
    </div>
    <div>
        <div class="form-row">
            <form action="" method="POST">
                <div class="col col-md-12">
                <div class="form-row ">
                        <div class="col col-md-5">
                            <input type="date" name="date1" value="<?php echo $date1 ?>" class="form-control" placeholder="First name">
                        </div>
                        <div class="col col-md-5">
                            <input type="date" name="date2" value="<?php echo $date2; ?>" class="form-control" placeholder="First name">
                        </div>
                        
                        <div class="col col-md-2">
                            <button class="btn btn-primary" name="lihatadj">Lihat</button>
                        </div>
                </div><br>
                </div>
            </form>
            <div class="col">
                <div class="form-row">
                    <div class="col col-md-4">
                        <a href="./index.php?route=tambahadj"><button class="btn btn-success float-left col-md-5">Buat Documen</button></a>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="card-body">
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No.</th>
                        <th>No. Adjustment</th>
                        <th>Tgl Adjustment</th>
                        <th>Dibuat Oleh</th>
                        <th>Penambahan Item</th>
                        <th>Pengurangan Item</th>
                        <th>Catatan</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          
                          $sel = "SELECT t_adjustment_id,tgl_adjustment,usercreated,sum(minus) as minus,
                          sum(plus) as plus,catatan FROM 
                          (
                          select 
                          b.t_adjustment_id,tgl_adjustment,usercreated
                          ,case when b.eksekusi = 'MINUS' then sum(qty) else 0 end minus
                          ,case when b.eksekusi = 'PLUS' then sum(qty) else 0 end plus
                          ,a.catatan 
                          from t_adjustment a
                          inner join t_adjustment_detail b on a.t_adjustment_id = b.t_adjustment_id
                          inner join m_barang c on c.m_barang_id = b.m_barang_id
                          inner join m_rak d on d.m_rak_id = b.m_rak_id $where -- AND B.eksekusi = 'minus'
                          group by  b.t_adjustment_id,b.eksekusi,tgl_adjustment,usercreated,a.catatan
                          )dt GROUP BY t_adjustment_id,tgl_adjustment,usercreated,catatan
                          ";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['t_adjustment_id']; ?></td>
                        <td><?php echo $res['tgl_adjustment']; ?></td>
                        <td><?php echo $res['usercreated']; ?></td>
                        <td><?php echo $res['plus']; ?></td>
                        <td><?php echo $res['minus']; ?></td>
                        <td><?php echo $res['catatan']; ?></td>
                        <td style="text-align:center">
                           <a href="./?route=adjdetail&id=<?php  echo $res['t_adjustment_id'] ?>"><button class="btn btn-success">Lihat</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
        </div>
    </div>
</div>

<script>
    function confirmation(delName){
    var del=confirm("Yakin Ingin menghapus PO ini..??");
    if (del==true){
        window.location.href="./pages/Transaksi PO/action.php?act=del&id="+delName;
    }
    return del;
}
</script>