<?php
    $gudang_desc = "";
    $gudang_id = "";
    if(isset($_POST['add'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $gudang_desc = $_POST['gudang_desc'];
            $gudang_id = $_POST['gudang_id'];
           
            $uniq = uniq();
            if($gudang_id != ""){
                $insert = "update m_gudang set gudang_desc = '$gudang_desc' where m_gudang_id = '$gudang_id'";
                // echo $insert;
                $gudang_desc = "";
                $gudang_id = "";
                echo "<script>window.location='./?route=gudang'</script>";
            }else{
                $insert = "insert into m_gudang (m_gudang_id,gudang_desc) values ('$uniq','$gudang_desc')";
            }
            $gudang = "";
            $gudang_id = "";
            $resInsert = mysqli_query($con,$insert);
            // echo "<script>window.location='./?route=user'</script>";
        }
    }

    if(isset($_GET['act'])){
        if($_GET['act'] == "del"){
            $id = $_GET['id'];
            $sql_del = "delete from m_gudang where m_gudang_id = '$id'";
            mysqli_query($con,$sql_del);
            echo "<script>window.location='./?route=gudang'</script>";
        }else if($_GET['act'] == "edit"){
            $id = $_GET['id'];
            $sel = "select * from m_gudang where m_gudang_id = '$id'";
            $qr = mysqli_query($con,$sel);
            $dtsel = mysqli_fetch_array($qr);
            $gudang_id = $dtsel['m_gudang_id'];
            $gudang_desc = $dtsel['gudang_desc'];
        }
        //;
    }

?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Master Gudang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Master</li>
        <li class="breadcrumb-item active" aria-current="page">Rak  /     Gudang</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-4">
                <form action="" method="POST" enctype="multipart/form-data">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                <input type="hidden" name = "gudang_id" value="<?php echo $gudang_id; ?>" name="randcheck" />
               
                <div class="form-group">
                    <input type="text" placeholder="Nama Gudang" name="gudang_desc" class="form-control" value="<?php echo $gudang_desc; ?>" >
                </div>
               
                <button type="submit" name="add" class="btn btn-primary btn-block col-md-4">Simpan</button>
                </form>
                <a href="./?route=rak">
                <button type="submit" class="btn btn-danger btn-block col-md-4 float-right" style="margin-top:-35px"> << Master Rak</button>
                </a>
            </div>
        </div>
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>Nomor</th>
                        <th>Nama Gudang</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          $sel = "select * from m_gudang ";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['gudang_desc']; ?></td>
                        <td style="text-align:center">
                            <a href="./?route=gudang&act=del&id=<?php echo $res['m_gudang_id']; ?>"><button class="btn btn-danger col-3">Hapus</button></a>
                            <a href="./?route=gudang&act=edit&id=<?php echo $res['m_gudang_id']; ?>"><button class="btn btn-warning col-3">Edit</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
                </div>
    </div>
</div>