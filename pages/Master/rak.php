<?php
    $rak = "";
    $gudang = "";
    $gudang_id = "";
    $rak_id = "";
    $kategori_id = "";
    $kategori = "";
    if(isset($_POST['add'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $gudang = $_POST['gudang_id'];
            $rak = $_POST['rak'];
            $rak_id = $_POST['rak_id'];
            $gudang_id = $_POST['gudang_id'];
            $kategori_id = $_POST['kategori'];
            $uniq = uniq();
            if($rak_id != ""){
                $insert = "update m_rak set m_gudang_id = '$gudang_id',rak_desc = '$rak',m_kategori_id = '$kategori_id'
                where m_rak_id = '$rak_id'";
                // echo $insert;
                $rak = "";
                $gudang = "";
                $gudang_id = "";
                $rak_id = "";
                echo "<script>window.location='./?route=rak'</script>";
            }else{
                $insert = "insert into m_rak (m_gudang_id,m_kategori_id,rak_desc) values ('$gudang','$kategori_id','$rak')";
            }
            $rak = "";
            $gudang = "";
            $gudang_id = "";
            $rak_id = "";
            $resInsert = mysqli_query($con,$insert);
            // echo "<script>window.location='./?route=user'</script>";
        }
    }

    if(isset($_GET['act'])){
        if($_GET['act'] == "del"){
            $id = $_GET['id'];
            $sql_del = "delete from m_rak where m_rak_id = '$id'";
            mysqli_query($con,$sql_del);
            echo "<script>window.location='./?route=rak'</script>";
        }else if($_GET['act'] == "edit"){
            $id = $_GET['id'];
            $sel = "select * from m_rak a
                inner join m_gudang b on a.m_gudang_id = b.m_gudang_id
                inner join m_kategori c on c.m_kategori_id = a.m_kategori_id
                where a.m_rak_id = '$id'";
            $qr = mysqli_query($con,$sel);
            $dtsel = mysqli_fetch_array($qr);
            $rak_id = $dtsel['m_rak_id'];
            $gudang_id = $dtsel['m_gudang_id'];
            $rak = $dtsel['rak_desc'];
            $gudang = $dtsel['gudang_desc'];
            $kategori = $dtsel['kategory_desc'];
            $kategori_id = $dtsel['m_kategori_id'];
            // $password = $dtsel['password'];
            // $m_user_id = $dtsel['m_user_id'];
        }
        //;
    }

?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Master Rak</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./?route">Home</a></li>
        <li class="breadcrumb-item">Master</li>
        <li class="breadcrumb-item active" aria-current="page">Rak</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-4">
                <form action="" method="POST" enctype="multipart/form-data">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                <input type="hidden" name = "rak_id" value="<?php echo $rak_id; ?>" name="randcheck" />
               
                <div class="form-group">
                    <input type="text" placeholder="Nama Rak" name="rak" class="form-control" value="<?php echo $rak; ?>" >
                </div>
                <div class="form-group">
                    <select name="gudang_id" class="form-control" id="sel1">
                 <?php
                    if($gudang == ""){
                 ?>
                    <option value="" disabled selected>Gudang</option>  
                 <?php
                    }else{
                 ?>
                    <option value="<?php  echo $gudang_id ?>"><?php  echo $gudang ?></option>  
                <?php
                    }
                 ?>
                        <?php
                            $varquery = "select * from m_gudang";
                            $res = mysqli_query($con,$varquery);
                            while($ds = mysqli_fetch_array($res)){
                        ?>
                            <option value="<?php echo $ds['m_gudang_id'] ?>"><?php echo $ds['gudang_desc'] ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <select name="kategori" class="form-control" id="sel1">
                 <?php
                    if($gudang == ""){
                 ?>
                    <option value="" disabled selected>Kategori</option>  
                 <?php
                    }else{
                 ?>
                    <option value="<?php echo $kategori_id ?>"><?php echo $kategori; ?></option>  
                <?php
                    }
                 ?>
                        <?php
                            $varquery = "select * from m_kategori";
                            $res = mysqli_query($con,$varquery);
                            while($ds = mysqli_fetch_array($res)){
                        ?>
                            <option value="<?php echo $ds['m_kategori_id'] ?>"><?php echo $ds['kategory_desc'] ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
               
                <button type="submit" name="add" class="btn btn-primary btn-block col-md-4">Simpan</button>
                <!-- <button type="submit" class="btn btn-warning btn-block col-md-4">Edit</button> -->
                </form>
                <a href="./?route=gudang">
                <button type="submit" class="btn btn-success btn-block col-md-4 float-right" style="margin-top:-37px"> + Tambah Gudang</button>
                </a>
                <a href="./?route=kategori">
                <button type="submit" class="btn btn-warning btn-block col-md-4 float-right" style="margin-top:-37px"> + Tambah Kategori</button>
                </a>
            </div>
        </div>
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>Nomor</th>
                        <th>Nama Gudang</th>
                        <th>Kategori</th>
                        <th>Rak</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          $sel = "select m_rak_id,kategory_desc,gudang_desc,rak_desc from m_gudang a
                          inner join m_rak b on a.m_gudang_id = b.m_gudang_id
                          inner join m_kategori c on c.m_kategori_id = b.m_kategori_id";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['gudang_desc']; ?></td>
                        <td><?php echo $res['kategory_desc']; ?></td>
                        <td><?php echo $res['rak_desc']; ?></td>
                        <td style="text-align:center">
                            <a href="./?route=rak&act=del&id=<?php echo $res['m_rak_id']; ?>"><button class="btn btn-danger col-3">Hapus</button></a>
                            <a href="./?route=rak&act=edit&id=<?php echo $res['m_rak_id']; ?>"><button class="btn btn-warning col-3">Edit</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
                </div>
    </div>
</div>