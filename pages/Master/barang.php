<?php
    $kodebarang = "";
    $namabarang = "";
    $gudangid = "";
    $namagudang = "";
    $rakid = "";
    $namarak = "";
    $satuanid = "";
    $satuandesc = "";
    $stok = ""; 

    if(isset($_POST['add'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $kodebarang = $_POST['kodebarang'];
            $namabarang = $_POST['namabarang'];
            $principle = $_POST['principle'];
            // $gudang = $_POST['gudang'];
            // $rak = $_POST['rak'];
            // $satuan = $_POST['satuan'];
            // $stok = $_POST['stok'];

            $insert = "insert into m_barang select FLOOR(100000 + RAND() * 89999),'$kodebarang','$principle','$namabarang','','',0";
            $resinsert = mysqli_query($con,$insert);
            if($resinsert){
                echo "<script>alert('Berhasil')</script>";
            }
            $principle = "";
            $kodebarang = "";
            $namabarang = "";
            $gudangid = "";
            $namagudang = "";
            $rakid = "";
            $namarak = "";
            $satuanid = "";
            $satuandesc = "";
            $stok = ""; 
           
        }
    }

    if(isset($_GET['act'])){
        if($_GET['act'] == "del"){
            $id = $_GET['id'];
            $sql_del = "delete from m_barang where m_barang_id = '$id'";
            mysqli_query($con,$sql_del);
            echo "<script>window.location='./?route=barang'</script>";
        }else if($_GET['act'] == "edit"){
            $id = $_GET['id'];
            $sel = "select * from m_barang where m_barang_id = '$id'";
            $qr = mysqli_query($con,$sel);
            $dtsel = mysqli_fetch_array($qr);
            $kodebarang = $dtsel['kode_barang'];
            $namabarang = $dtsel['nama_barang'];

            if( isset($_POST['kodebarang'])){
                $idx = $_GET['id'];
                $kodebarang = $_POST['kodebarang'];
                $namabarang = $_POST['namabarang'];
                $x = "update m_barang set kode_barang = '$kodebarang', nama_barang = '$namabarang' where m_barang_id = '$idx'";
                mysqli_query($con,$x);
                // echo "<script>window.location('./?route=barang')</script>";

                // echo  $x;
                echo "<script>window.location='./?route=barang'</script>";
            };
        }
    }

?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Master Barang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Master</li>
        <li class="breadcrumb-item active" aria-current="page">Master Barang</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-4">
                
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                <input type="hidden" name = "kode_barang" value="<?php echo $kodebarang; ?>" name="randcheck" />
                <div class="form-group">
                    <input type="text" placeholder="Kode Barang" name="kodebarang" class="form-control" id="exampleInputFirstName" value="<?php echo $kodebarang; ?>" >
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Barang" name="namabarang" class="form-control" id="exampleInputFirstName" value="<?php echo $namabarang; ?>" >
                </div>
                <div class="form-group">
                    <select name="principle" id=""  class="form-control" >
                        <option value="">-- Plih Principle --</option>
                        <?php
                            $res = mysqli_query($con,"select * from m_principle");
                            while($d = mysqli_fetch_array($res)){
                        ?>
                        <option value="<?php echo $d['m_principle_id'] ?>"><?php echo $d['principle_desc'] ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-4">
                <?php
                    if(isset($_GET['act'])){
                        if($_GET['act'] == "edit"){
                ?>
                        <button type="submit" class="btn btn-warning btn-block col-md-4">Edit</button>
                <?php
                        }
                    }else{
                ?>
                        <button type="submit" name="add" class="btn btn-primary btn-block col-md-4">Simpan</button>
                <?php
                    }

                ?>
                
                <!-- > -->
                
            </div>
        </div>
        </form>
        <a href="./pages/Master/exportdata.php?read=barang">
        <button type="submit" name="add" class="btn btn-danger btn-block col-md-1 float-center">Export Data</button>
        </a>
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>Nomor</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Nama Principle</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          $sel = "select m_barang_id,kode_barang,nama_barang,satuan_desc,a.m_satuan_id,principle_desc,d.m_gudang_id,d.gudang_desc,a.m_rak_id,rak_desc
                          ,stok
                          from m_barang a
                          left join m_principle x on x.m_principle_id = a.m_principle_id
                          left join m_satuan b on a.m_satuan_id = b.m_satuan_id
                          left join m_rak c on c.m_rak_id = a.m_rak_id
                          left join m_gudang d on d.m_gudang_id = c.m_gudang_id order by nama_barang";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['kode_barang']; ?></td>
                        <td><?php echo $res['nama_barang']; ?></td>
                        <td><?php echo $res['principle_desc']; ?></td>
                        <td style="text-align:center">
                            <a href="./?route=barang&act=del&id=<?php echo $res['m_barang_id']; ?>"><button class="btn btn-danger">Hapus</button></a>
                            <a href="./?route=barang&act=edit&id=<?php echo $res['m_barang_id']; ?>"><button class="btn btn-warning">Edit</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
        </div>
    </div>
</div>