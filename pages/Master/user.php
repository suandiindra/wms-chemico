<?php
    $username = "";
    $email = "";
    $jabatan = "";
    $jabatan_id = "";
    $password = "";
    $m_user_id = "";

    if(isset($_POST['add'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $username = $_POST['username'];
            $email = $_POST['email'];
            $jabatan = $_POST['jabatan'];
            // $password = $_POST['password'];
            $password = "password";
            if($_POST['password']){
                $password = md5($_POST['password']);
            }

            $m_user_id = $_POST['m_user_id'];
            $uniq = uniq();
            if($m_user_id != ""){
                if($_POST['password']){
                    $insert = "update m_user set user_name = '$username',email = '$email',m_jabatan_id = '$jabatan'
                    ,password = '$password' where m_user_id = '$m_user_id'";
                }else{
                    $insert = "update m_user set user_name = '$username',email = '$email',m_jabatan_id = '$jabatan'
                     where m_user_id = '$m_user_id'";
                }
                $resInsert = mysqli_query($con,$insert);
            }else{
                if($username || $jabatan){
                    $insert = "insert into m_user (m_user_id,user_name,email,m_jabatan_id,password) values ($uniq,'$username','$email','$jabatan','$password')";
                    $resInsert = mysqli_query($con,$insert);
                }
            }

            echo "<script>window.location='./?route=user'</script>";
        }
    }

    if(isset($_GET['act'])){
        if($_GET['act'] == "del"){
            $id = $_GET['id'];
            $sql_del = "delete from m_user where m_user_id = '$id'";
            mysqli_query($con,$sql_del);
            echo "<script>window.location='./?route=user'</script>";
        }else if($_GET['act'] == "edit"){
            $id = $_GET['id'];
            $sel = "select m_user_id,user_name,email,jabatan_desc,a.m_jabatan_id,password from m_user a
                    inner join m_jabatan b on a.m_jabatan_id = b.m_jabatan_id where m_user_id = '$id'";
            $qr = mysqli_query($con,$sel);
            $dtsel = mysqli_fetch_array($qr);
            $username = $dtsel['user_name'];
            $email = $dtsel['email'];
            $jabatan = $dtsel['jabatan_desc'];
            $jabatan_id = $dtsel['m_jabatan_id'];
            // $password = $dtsel['password'];
            $m_user_id = $dtsel['m_user_id'];
        }
    }

?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Master User</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Master</li>
        <li class="breadcrumb-item active" aria-current="page">User</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-4">
                <form action="" method="POST" enctype="multipart/form-data">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                <input type="hidden" name = "m_user_id" value="<?php echo $m_user_id; ?>" name="randcheck" />
                <div class="form-group">
                    <input type="text" placeholder="User Name" name="username" class="form-control" id="exampleInputFirstName" value="<?php echo $username; ?>" >
                </div>
                <div class="form-group">
                    <input type="text" value="<?php echo $email; ?>"  placeholder="Email" name="email" class="form-control" id="exampleInputLastName" >
                </div>
                <div class="form-group">
                    <select name="jabatan" class="form-control" id="sel1">
                 <?php
                    if($jabatan == ""){
                 ?>
                    <option value="" disabled selected>Jabatan</option>  
                 <?php
                    }else{
                 ?>
                    <option value="<?php  echo $jabatan_id ?>"><?php  echo $jabatan ?></option>  
                <?php
                    }
                 ?>
                    <!-- test -->
                        <?php
                            $varquery = "select * from m_jabatan";
                            $res = mysqli_query($con,$varquery);
                            while($ds = mysqli_fetch_array($res)){
                        ?>
                            <option value="<?php echo $ds['m_jabatan_id'] ?>"><?php echo $ds['jabatan_desc'] ?></option>
                        <?php
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" value="<?php echo $password; ?>"  placeholder="Password" name="password" class="form-control" id="exampleInputLastName" >
                </div>
                <button type="submit" name="add" class="btn btn-primary btn-block col-md-4">Simpan</button>
                <!-- <button type="submit" class="btn btn-warning btn-block col-md-4">Edit</button> -->
                </form>
            </div>
        </div>
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>Nomor</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Jabatan</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          $sel = "select m_user_id,user_name,email,jabatan_desc from m_user a
                          inner join m_jabatan b on a.m_jabatan_id = b.m_jabatan_id";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['user_name']; ?></td>
                        <td><?php echo $res['email']; ?></td>
                        <td><?php echo $res['jabatan_desc']; ?></td>
                        <td style="text-align:center">
                            <a href="./?route=user&act=del&id=<?php echo $res['m_user_id']; ?>"><button class="btn btn-danger col-3">Hapus</button></a>
                            <a href="./?route=user&act=edit&id=<?php echo $res['m_user_id']; ?>"><button class="btn btn-warning col-3">Edit</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
                </div>
    </div>
</div>