<?php
     session_start();
     $m_user_id = $_SESSION['user_id'];
     include("../../utility/config.php");
     include("../../utility/fungsi.php");
    $read = $_GET['read'];
    $tgl = date("Ymd");
    $filename = $read."-".$tgl;
    echo "<script>window.location='../../?route=expired'</script>";
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=$filename.xls");

if($read == "barang"){
?>

<table border=1>
<thead class="thead-light">
    <tr>
    <th>Nomor</th>
    <th>Kode Barang</th>
    <th>Nama Barang</th>
    <th>Nama Principle</th>
    </tr>
</thead>
<tbody>
    <?php 
        $sel = "select m_barang_id,kode_barang,nama_barang,satuan_desc,a.m_satuan_id,principle_desc,d.m_gudang_id,d.gudang_desc,a.m_rak_id,rak_desc
        ,stok
        from m_barang a
        left join m_principle x on x.m_principle_id = a.m_principle_id
        left join m_satuan b on a.m_satuan_id = b.m_satuan_id
        left join m_rak c on c.m_rak_id = a.m_rak_id
        left join m_gudang d on d.m_gudang_id = c.m_gudang_id order by nama_barang";
        $result = mysqli_query($con,$sel);
        $i = 1;
        while($res = mysqli_fetch_array($result)){
    ?>
    <tr>
    <td><?php echo $i; ?></td>
    <td><?php echo $res['kode_barang']; ?></td>
    <td><?php echo $res['nama_barang']; ?></td>
    <td><?php echo $res['principle_desc']; ?></td>
    </tr>
    <?php
        $i =  $i + 1;
        }
    ?>
</tbody>
</table>
<?php
}elseif($read == "principle"){
?>
    <table border=1>
    <thead class="thead-light">
        <tr>
        <th>Nomor</th>
        <th>Kode Principle</th>
        <th>Nama Principle</th>
        <!-- <th>PIC</th> -->
        <th>Kontak</th>
        <th>Alamat</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $sel = "select * from m_principle order by cast(m_principle_id as int) asc";
            $result = mysqli_query($con,$sel);
            $i = 1;
            while($res = mysqli_fetch_array($result)){
        ?>
        <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $res['kode_principle']; ?></td>
        <td><?php echo $res['principle_desc']; ?></td>
        <!-- <td><?php echo $res['pic']; ?></td> -->
        <td><?php echo $res['kontak']; ?></td>
        <td><?php echo $res['alamat']; ?></td>
        </tr>
        <?php
            $i =  $i + 1;
            }
        ?>
    </tbody>
    </table>

<?php
}elseif($read == "customer"){
?>
    <table border=1>
    <thead class="thead-light">
        <tr>
        <th>Nomor</th>
        <th>Nama Customer</th>
        <th>Kontak</th>
        <th>PIC</th>
        <th>Alamat</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $sel = "select * from m_customer";
            $result = mysqli_query($con,$sel);
            $i = 1;
            while($res = mysqli_fetch_array($result)){
        ?>
        <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $res['customer_desc']; ?></td>
        <td><?php echo $res['kontak']; ?></td>
        <td><?php echo $res['PIC']; ?></td>
        <td><?php echo $res['alamat']; ?></td>
        </tr>
        <?php
            $i =  $i + 1;
            }
        ?>
    </tbody>
    </table>
<?php
}elseif($read == "vendor"){
?>
    <table border=1>
    <thead class="thead-light">
        <tr>
        <th>Nomor</th>
        <th>Kode Vendor</th>
        <th>Nama Vendor</th>
        <th>PIC</th>
        <th>Kontak</th>
        <th>Alamat</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $sel = "select * from m_vendor";
            $result = mysqli_query($con,$sel);
            $i = 1;
            while($res = mysqli_fetch_array($result)){
        ?>
        <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $res['kode_vendor']; ?></td>
        <td><?php echo $res['vendor_desc']; ?></td>
        <td><?php echo $res['PIC']; ?></td>
        <td><?php echo $res['nomor_tlp']; ?></td>
        <td><?php echo $res['alamat']; ?></td>
        </tr>
        <?php
            $i =  $i + 1;
            }
        ?>
    </tbody>
    </table>
<?php
}

?>
