<?php
    $id_princ = "";
    $principleid = "";
    $principledesc = "";
    $kontak = "";
    $alamat = "";
    

    if(isset($_POST['add'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $principleid = $_POST['principleid'];
            $principledesc = $_POST['principledesc'];
            $kontak = $_POST['kontak'];
            $alamat = $_POST['alamat'];
            $id_princ = $_POST['id_princ'];
            
            if($id_princ != ""){
                $update = "update m_principle set kode_principle = '$principleid' , principle_desc = '$principledesc'
                ,kontak = '$kontak', alamat = '$alamat' where m_principle_id = '$id_princ'";
                $resinsert = mysqli_query($con,$update);
                echo "<script>window.location='./?route=principal'</script>";
            }else{
                $insert = "insert into m_principle
                select FLOOR(100000 + RAND() * 89999),'$principleid','$principledesc','$kontak','$alamat','',now()";
                // echo $insert;
                $resinsert = mysqli_query($con,$insert);
                if($resinsert){
                    echo "<script>alert('Berhasil')</script>";
                }
            }
            $id_princ = "";
            $principleid = "";
            $principledesc = "";
            $kontak = "";
            $alamat = "";
           
        }
    }

    if(isset($_GET['act'])){
        if($_GET['act'] == "del"){
            $id = $_GET['id'];
            $sql_del = "delete from m_principle where m_principle_id = '$id'";
            mysqli_query($con,$sql_del);
            echo "<script>window.location='./?route=principal'</script>";
        }else if($_GET['act'] == "edit"){
            $id = $_GET['id'];
            $sel = "select * from m_principle where m_principle_id = '$id'";
            $qr = mysqli_query($con,$sel);
            $dtsel = mysqli_fetch_array($qr);
            $id_princ = $dtsel['m_principle_id'];
            $principleid = $dtsel['kode_principle'];
            $principledesc = $dtsel['principle_desc'];
            $kontak = $dtsel['kontak'];
            $alamat = $dtsel['alamat'];
            // $m_user_id = $dtsel['m_user_id'];
        }
    }

?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Master Principle</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Master</li>
        <li class="breadcrumb-item active" aria-current="page">Master Principle</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-4">
                
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                <input type="hidden" name = "id_princ" value="<?php echo $id_princ; ?>" />
                <div class="form-group">
                    <input type="text" placeholder="Principle ID" name="principleid" class="form-control" id="exampleInputFirstName" value="<?php echo $principleid; ?>" >
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Principle Desc" name="principledesc" class="form-control" id="exampleInputFirstName" value="<?php echo $principledesc; ?>" >
                </div>
                <!-- <div class="form-group">
                    <input type="text" placeholder="PIC" name="pic" class="form-control" id="exampleInputFirstName" value="<?php echo $pic; ?>" >
                </div> -->
                
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <input type="text" placeholder="Kontak" name="kontak" class="form-control" id="exampleInputFirstName" value="<?php echo $kontak; ?>" >
                </div>
                <div class="form-group">
                <textarea class="form-control" placeholder="Alamat"  name="alamat" id="exampleFormControlTextarea1" rows="3" ><?php echo $alamat; ?></textarea>
                    <!-- <input type="text" placeholder="Alamat" name="alamat" class="form-control" id="exampleInputFirstName" value="<?php echo $alamat; ?>" > -->
                </div>
                
            </div>
            <div class="col-lg-4">
                
                <button type="submit" name="add" class="btn btn-primary btn-block col-md-4">Simpan</button>
                
            </div>
        </div>
        </form>
        <a href="./pages/Master/exportdata.php?read=principle">
        <button type="submit" class="btn btn-danger btn-block col-md-1">Export Data</button>
        </a>
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>Nomor</th>
                        <th>Kode Principle</th>
                        <th>Nama Principle</th>
                        <!-- <th>PIC</th> -->
                        <th>Kontak</th>
                        <th>Alamat</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          $sel = "select * from m_principle order by cast(m_principle_id as int) asc";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['kode_principle']; ?></td>
                        <td><?php echo $res['principle_desc']; ?></td>
                        <!-- <td><?php echo $res['pic']; ?></td> -->
                        <td><?php echo $res['kontak']; ?></td>
                        <td><?php echo $res['alamat']; ?></td>
                        <td style="text-align:center">
                            <a href="./?route=principal&act=del&id=<?php echo $res['m_principle_id']; ?>"><button class="btn btn-danger ">Hapus</button></a>
                            <a href="./?route=principal&act=edit&id=<?php echo $res['m_principle_id']; ?>"><button class="btn btn-warning ">Edit</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
                </div>
    </div>
</div>