<?php
    $id_customer = "";
    $kodecustomer = "";
    $customerdesc = "";
    $kontak = "";
    $alamat = "";
    $pic = "";
    

    if(isset($_POST['add'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            
            $customerdesc = $_POST['customerdesc'];
            $kontak = $_POST['kontak'];
            $alamat = $_POST['alamat'];
            $id_customer = $_POST['id_customer'];
            $pic = $_POST['pic'];
            
            if($id_customer != ""){
                $update = "update m_customer set customer_desc = '$customerdesc'
                ,kontak = '$kontak', alamat = '$alamat',pic='$pic' where m_customer_id = '$id_customer'";
                $resinsert = mysqli_query($con,$update);
                echo "<script>window.location='./?route=customer'</script>";
                // echo $update;
            }else{
                $kodecustomer = sprintf("%04d", getCountTable("m_customer",$con));
                $insert = "insert into m_customer
                select FLOOR(100000 + RAND() * 89999),'$kodecustomer','$customerdesc','$kontak','$pic','$alamat',now()";
                $resinsert = mysqli_query($con,$insert);
                if($resinsert){
                    echo "<script>alert('Berhasil')</script>";
                }
            }
            $id_customer = "";
            $kodecustomer = "";
            $customerdesc = "";
            $kontak = "";
            $alamat = "";
            // echo $kodecustomer;
           
        }
    }

    if(isset($_GET['act'])){
        if($_GET['act'] == "del"){
            $id = $_GET['id'];
            $sql_del = "delete from m_customer where m_customer_id = '$id'";
            mysqli_query($con,$sql_del);
            echo "<script>window.location='./?route=customer'</script>";
        }else if($_GET['act'] == "edit"){
            $id = $_GET['id'];
            $sel = "select * from m_customer where m_customer_id = '$id'";
            $qr = mysqli_query($con,$sel);
            $dtsel = mysqli_fetch_array($qr);
            $id_customer = $dtsel['m_customer_id'];
            $kodecustomer = $dtsel['kode_customer'];
            $customerdesc = $dtsel['customer_desc'];
            $kontak = $dtsel['kontak'];
            $alamat = $dtsel['alamat'];
            $pic = $dtsel['PIC'];
            // $m_user_id = $dtsel['m_user_id'];
        }
    }

?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Master Customer</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Master</li>
        <li class="breadcrumb-item active" aria-current="page">Master Customer</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-4">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                <input type="hidden" name = "id_customer" value="<?php echo $id_customer; ?>" />
                <div class="form-group">
                    <input type="text" placeholder="Customer Desc" name="customerdesc" class="form-control" id="exampleInputFirstName" value="<?php echo $customerdesc; ?>" >
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Kontak" name="kontak" class="form-control" id="exampleInputFirstName" value="<?php echo $kontak; ?>" >
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                <input type="text" placeholder="PIC" name="pic" class="form-control" id="exampleInputFirstName" value="<?php echo $pic; ?>" >
                </div>
                <div class="form-group">
                    <textarea class="form-control" placeholder="Alamat"  name="alamat" id="exampleFormControlTextarea1" rows="3" ><?php echo $alamat; ?></textarea>
                </div>
                
            </div>
            <div class="col-lg-4">
                <button type="submit" name="add" class="btn btn-primary btn-block col-md-4">Simpan</button>
                <!-- <button type="submit" class="btn btn-warning btn-block col-md-4">Edit</button> -->
            </div>
        </div>
        </form>
        <a href="./pages/Master/exportdata.php?read=customer">
        <button type="submit" name="add" class="btn btn-danger btn-block col-md-1 float-center">Export Data</button>
        </a>
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>Nomor</th>
                        <th>Nama Customer</th>
                        <th>Kontak</th>
                        <th>PIC</th>
                        <th>Alamat</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          $sel = "select * from m_customer";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['customer_desc']; ?></td>
                        <td><?php echo $res['kontak']; ?></td>
                        <td><?php echo $res['PIC']; ?></td>
                        <td><?php echo $res['alamat']; ?></td>
                        <td style="text-align:center">
                            <a href="./?route=customer&act=del&id=<?php echo $res['m_customer_id']; ?>"><button class="btn btn-danger ">Hapus</button></a>
                            <a href="./?route=customer&act=edit&id=<?php echo $res['m_customer_id']; ?>"><button class="btn btn-warning ">Edit</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
                </div>
    </div>
</div>