<?php
    session_start();
    $m_user_id = $_SESSION['user_id'];
    include("../../utility/config.php");
    include("../../utility/fungsi.php");


    if(isset($_POST['detailbtn'])){
        $kode = $_POST['kode'];
        $batch = $_POST['batch'];
        $rak   = $_POST['rakid'];
        $rak_src = $_POST['rakbefore'];

        $q_stok_id = mysqli_query($con,"select * from t_stok where m_barang_id = '$kode' and batch = '$batch' and m_rak_id = '$rak_src'");
        $dt = mysqli_fetch_array($q_stok_id);
        $stok_id = $dt['t_stok_id'];
        $jml   = str_replace(',','.',$_POST['jml']);
        $catatan = $_POST['catatan'];
        $stok_awal = $dt['last_stok'];

        $sisa = cekSisa($con,$kode,$batch,$rak_src,$jml);

        // echo $sisa;
        if($sisa >= 0){
            $unik  = uniq();
            $sd = "select t_mutasi_id from t_mutasi where usercreated = '$m_user_id' and isconfirm = 0";
            
            $dt = querytodataset($con,$sd);
            // echo $sd."<br/>";
            if(strlen($dt['t_mutasi_id']) > 0){
                //$sel1 = "select * from t_stok where m_barang_id = '$kode' and batch = '$batch' ";
                $sel1 = "select * from t_stok where t_stok_id = '$stok_id'";
                $qr = mysqli_query($con,$sel1);
                $dtx = mysqli_fetch_array($qr);
                $source = $dtx['m_rak_id'];
                $unik = $dt['t_mutasi_id'];
                $sel = "insert into t_mutasi_detail 
                select '$unik','$stok_id','$kode','$batch','$source','$rak','$stok_awal','$jml',0,'$catatan',now()";
                
                // echo $sel;
                $res2 = mysqli_query($con,$sel);
                if($res2){
                    echo "<script>window.location='../../?route=tambahmutasi'</script>";
                }
            }else{
                $tgl = "now()";
                if(isset($_POST['mutasi_date'])){
                    $tgl = $_POST['mutasi_date'];
                }
                $insert = "insert into t_mutasi 
                select '$unik',$tgl,'$unik','$m_user_id','0','',now()";
                $res = mysqli_query($con,$insert);
                if($res){
                    //$sel1 = "select * from t_stok where m_barang_id = '$kode' and batch = '$batch'";
                    $sel1 = "select * from t_stok where t_stok_id = '$stok_id'";
                    $qr = mysqli_query($con,$sel1);
                    $dt = mysqli_fetch_array($qr);
                    $source = $dt['m_rak_id'];
                    $sel = "insert into t_mutasi_detail 
                    select '$unik','$stok_id','$kode','$batch','$source','$rak','$stok_awal','$jml',0,'$catatan',now()";
                    
                    $res2 = mysqli_query($con,$sel);
                    if($res2){
                        echo "<script>window.location='../../?route=tambahmutasi'</script>";
                    }
                }
            }
        }else{
            echo "<script>alert('Stok gudang tidak mencukupi')</script>";
            echo "<script>window.location='../../?route=tambahmutasi'</script>";
        }
        
    }elseif(isset($_GET['act'])){
        $id = $_GET['id'];
        $kode = $_GET['kodebarang'];
        $batch = $_GET['batch'];
        $del = "delete from t_mutasi_detail where t_mutasi_id = '$id' and m_barang_id = '$kode' and batch = '$batch'";
        $res2 = mysqli_query($con,$del);
        if($res2){
            $sec = "select count(*) as jml from t_mutasi_detail where t_mutasi_id = '$id'";
            $resx = mysqli_query($con,$sec);
            $dt = mysqli_fetch_array($resx);
            if($dt['jml'] == 0){
                $del = "delete from t_mutasi where t_mutasi_id = '$id'";
                $res2 = mysqli_query($con,$del);
            }
            echo "<script>window.location='../../?route=tambahmutasi'</script>";
        }
    }elseif(isset($_POST['confirmasi'])){
        $notes = $_POST['catatan'];
        $sel = "select b.m_barang_id,b.t_mutasi_id,t_stok_id,batch,m_barang_id,m_rak_id_source,m_rak_id_dest,b.jumlah from t_mutasi a
            inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
            where a.usercreated = '$m_user_id' and isconfirm = 0";
        $res = mysqli_query($con,$sel);
        while($da = mysqli_fetch_array($res)){
            $kodebarang = $da['m_barang_id'];
            $batch      = $da['batch'];
            $dtx        = querytodataset($con,"select * from t_stok where m_barang_id = '$kodebarang' and batch = '$batch'");
            $expired    = $dtx['expired'];
            $sumber     = $da['m_rak_id_source'];
            $tujuan     = $da['m_rak_id_dest'];
            $jumlah     = $da['jumlah'];
            $t_stok_id = $da['t_stok_id'];

            $updatestok = "update t_stok set qty_keluar = qty_keluar + $jumlah , last_stok = last_stok - $jumlah
            where t_stok_id = '$t_stok_id'";

            // echo $updatestok;
            mysqli_query($con,$updatestok);
            $lihat = "select count(*)cnt from t_stok where m_barang_id = '$kodebarang' and batch = '$batch' and m_rak_id = '$tujuan'";
            // echo $lihat;
            $dt = querytodataset($con,$lihat);
            if($dt['cnt'] > 0){
                $updatestok = "update t_stok set qty_masuk = qty_masuk + $jumlah , last_stok = last_stok + $jumlah
                where m_barang_id = '$kodebarang' and batch = '$batch' and m_rak_id = '$tujuan'";
                // echo $updatestok;
                mysqli_query($con,$updatestok);
            }else{
                $uniq = uniq();
                $masuk = "insert into t_stok 
                (m_barang_id,batch,expired,m_rak_id,stok_awal,qty_masuk,qty_adj_masuk,qty_keluar,qty_adj_keluar,last_stok,last_update)
                select '$kodebarang','$batch','$expired','$tujuan','0','$jumlah','0','0','0','$jumlah',now()";
                mysqli_query($con,$masuk);
            }
        //     // echo $updatestok;
        }
        $tgl = "now()";
        if(isset($_POST['mutasi_date'])){
            $tgl = $_POST['mutasi_date'];
        }

        $updateflag = "update t_mutasi set tgl_mutasi = '$tgl', isconfirm = 1,catatan='$notes' where usercreated = '$m_user_id' and isconfirm = 0";
        mysqli_query($con,$updateflag);
        echo "<script>window.location='../../?route=mutasi'</script>";
    }
    
?>