<?php
    $m_user_id  = $_SESSION['user_id'];
    $nomutasi = "";
?>
<script type="text/javascript">
    $(document).ready(function(){
    //     $("#kode").change(function(){
    //         var kode = $("#kode").val();
    //         console.log(kode);
    //         $.ajax({
    //             url:'pages/Mutasi/ajaxData.php',
    //             method : 'post',
    //             data : 'kode= ' + kode 
    //         }).done(function(value){
    //             console.log(value)
    //             $('#batch').html(value);
    //         })
    //     })

        $("#batch").change(function(){
            var kode = $("#batch").val();
            var kodebrg = $("#kode").val();
            // console.log(kode);
            $("#stok").text("");
            $.ajax({
                url:'pages/Mutasi/ajaxData.php',
                method : 'post',
                data : 'kode_batch= ' + kode +'&kode_barang= '+kodebrg
            }).done(function(vl){
                console.log(vl)
                $('#rakbefore').html(vl);

            })
        })
        $("#rakbefore").change(function(){
            var rak = $("#rakbefore").val();
            var m_barang = document.getElementById("kode").value;
            var batch = document.getElementById("batch").value;
            // console.log("sddddddddddddddd");
            $.ajax({
                url:'pages/Keluar/ajaxData.php',
                method : 'post',
                data : 'rak= ' + rak +"&barang= "+m_barang+"&batch="+batch
            }).done(function(vl){
                var value = vl.split("#")
                console.log(value[0])
                $("#stok").text('Stok = '+value[1]);
                // $('#rak_').html(vl);
                // $('#rak').val(vl);
            })
        })
    })
</script>
<?php
    $cek = "";
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Transfer Antar Gudang Rak</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Input PO</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-8">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
            </div>
        </div>
        
        <form action="./pages/Mutasi/actionmutasi.php" method="POST">
        <div class="form-row md-col-12">  
            <div class="col">
                <div class="form-group">
                    <!-- <label for="">Barang</label> -->
                    <select name="kode" id="kode" class="chosenbarang" style="margin-top:220px">
                    <option value="">-- Pilih --</option>
                    <?php
                        $s = "select a.m_barang_id,nama_barang from t_stok a
                        inner join m_barang b on a.m_barang_id = b.m_barang_id
                        group by a.m_barang_id,nama_barang;";
                        $res = mysqli_query($con,$s);
                        while($dt = mysqli_fetch_array($res)){
                    ?>
                        <option value="<?php echo $dt['m_barang_id'] ?>"><?php echo $dt['nama_barang']; ?></option>
                    <?php
                        }

                    ?>
                </select>
                </div>
                <u><span id="prc1" style="color:red;font-size:12px; margin-top:-10px"></span></u>
            </div>
            <div class="col col-md-1">
                <div class="form-group">
                <!-- <label for="">Batch</label> -->
                <select name="batch" id="batch" class="form-control">
                    <option value="">-- Batch --</option>
                </select>
                </div>
            </div>
            <div class="col col-md-2">
                <div class="form-group">
                <!-- <label for="">Rak Sebelumnya</label> -->
                <select name="rakbefore" id="rakbefore" class="form-control">
                    <option value="">-- Rak Asal --</option>
                </select>
                </div>
                <span id="stok" style="color:blue;font-size:12px; margin-top:-10px"></span>
            </div>
            <div class="col">
                <div class="form-group">
                    <!-- <label for="">Gudang Rak Pindah</label> -->
                    <select name="rakid" id="rakid" class="form-control">
                    <option value="">-- Rak Tujuan --</option>
                    <?php
                        $s = "SELECT * FROM m_gudang a
                        inner join m_rak b on a.m_gudang_id = b.m_gudang_id;";
                        $res = mysqli_query($con,$s);
                        while($dt = mysqli_fetch_array($res)){ 
                    ?>
                        <option value="<?php echo $dt['m_rak_id'] ?>"><?php echo $dt['rak_desc']; ?></option>
                    <?php
                        }

                    ?>
                </select>
                </div>
            </div>
            <div class="col col-md-1">
                <div class="form-group">
                <!-- <label for="">Total Qty</label> -->
                <input type="text"  name="jml" required placeholder="(Kg)" class="form-control" id="exampleInputFirstName" > 
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                <!-- <label for="">Catatan</label> -->
                <input type="text"  name="catatan" required placeholder="" class="form-control" id="exampleInputFirstName" > 
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                     <BUTTon class="btn btn-primary"  style="margin-top:0px" name="detailbtn">Add</BUTTon>
                </div>
            </div>
        </div>
        </form>
        <hr>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Item</th>
            <th>Batch</th>
            <th>Rak Asal</th>
            <th>Rak Tujuan</th>
            <th>Stok Asal</th>
            <th>QTY (Kg)</th>
            <th>Sisa</th>
            <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $sel = "select b.t_mutasi_id,c.m_barang_id,c.nama_barang,b.batch,d.rak_desc,b.jumlah,b.m_rak_id_dest,stok_awal 
                ,stok_awal - b.jumlah as sisa from t_mutasi a
                inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
                inner join m_barang c on c.m_barang_id = b.m_barang_id
                left join m_rak d on d.m_rak_id = b.m_rak_id_source
                where a.usercreated = '$m_user_id' and a.isconfirm = 0";
                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
            ?>
            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['batch']; ?></td>
            <td><?php echo $res['rak_desc']; ?></td>
            <td><?php echo cekrak($con,$res['m_rak_id_dest']); ?></td>
            <td><?php echo format($res['stok_awal']) ?></td>
            <td><?php echo format($res['jumlah']) ?></td>
            <td><?php echo format($res['sisa']) ?></td>
            <td style="text-align:center">
                <a href="./pages/Mutasi/actionmutasi.php?act=del&id=<?php echo $res['t_mutasi_id']; ?>&kodebarang=<?php  echo $res['m_barang_id']; ?>&batch=<?php echo $res['batch']; ?>"><button class="btn btn-warning">Hapus</button></a>
            </td>
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
        <hr>
        <form action="./pages/Mutasi/actionmutasi.php" method="POST">
        <p>Tanggal Mutasi</p>
        <input type="date" class="form-control col-md-2" name="mutasi_date"/><br>
        <textarea class="form-control col-md-6" placeholder="Catatan Mutasi Barang . . ." name="catatan" id="" cols="30" rows="3"></textarea><br>
        <button class="btn btn-success" name="confirmasi">Konfirmasi Mutasi</button>
        </form>
    </div>
</div>

<script>
    $(".chosenbarang").chosen();
    
    $("#kode").on('change',function(){
        var kode = $("#kode").val();
        console.log(kode);
        $.ajax({
            url:'pages/Mutasi/ajaxData.php',
            method : 'post',
            data : 'kode= ' + kode 
        }).done(function(value){
            console.log(value)
            value = value.split("#")
            $('#batch').html(value[0]);
            $("#prc1").text(value[1]);
            $("#stok").text("");
        })
    })
</script>