<?php
    $m_user_id  = $_SESSION['user_id'];
    $nomutasi = $_GET['id'];
    $sel = "select a.tgl_mutasi,a.nomor_mutasi,b.t_mutasi_id,c.m_barang_id,c.nama_barang,b.batch,d.rak_desc,b.jumlah,b.m_rak_id_dest from t_mutasi a
    inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
    inner join m_barang c on c.m_barang_id = b.m_barang_id
    inner join m_rak d on d.m_rak_id = b.m_rak_id_source
    where a.nomor_mutasi = '$nomutasi'";
    // echo $sel;
    $result = mysqli_query($con,$sel);
    $dx = mysqli_fetch_array($result);
?>

<?php
    $cek = "";
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Transfer Antar Gudang Rak</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Input PO</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
        <div class="row col-md-4">
            <table>
                <tr>
                    <td style="width:150px">No Dokumen</td>
                    <td style="width:50px">:</td>
                    <td><?php echo $dx['nomor_mutasi'] ?></td>
                </tr>
                <tr>
                    <td style="width:150px">Tgl Dokumen</td>
                    <td style="width:50px">:</td>
                    <td><?php echo $dx['tgl_mutasi'] ?></td>
                </tr>
            </table>
        </div>
        
        <hr>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Item</th>
            <th>Batch</th>
            <th>Rak Asal</th>
            <th>Rak Tujuan</th>
            <th>QTY (Kg)</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 1;
                $sel = "select a.tgl_mutasi,a.nomor_mutasi,b.t_mutasi_id,c.m_barang_id,c.nama_barang,b.batch,d.rak_desc,b.jumlah,b.m_rak_id_dest from t_mutasi a
                inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
                inner join m_barang c on c.m_barang_id = b.m_barang_id
                inner join m_rak d on d.m_rak_id = b.m_rak_id_source
                where a.nomor_mutasi = '$nomutasi'";
                $result1 = mysqli_query($con,$sel);
                while($res = mysqli_fetch_array($result1)){
            ?>
            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['batch']; ?></td>
            <td><?php echo $res['rak_desc']; ?></td>
            <td><?php echo cekrak($con,$res['m_rak_id_dest']); ?></td>
            <td><?php echo format($res['jumlah']) ?></td>
            
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
      
    </div>
</div>