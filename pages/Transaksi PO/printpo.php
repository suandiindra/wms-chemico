<!DOCTYPE html>
<html>
<head>
  <title>PT. CHEMICO SURABAYA</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body onload="window.print()">
<div class="d-flex flex-row justify-content-center align-items-center" style="height: 70px;">
    <div class="p-2">
        <h4>PURCHASE ORDER</h4>
    </div>
</div>
<?php
    include("../../utility/config.php");
    $id = $_GET['id'];
    $sel = "select a.t_po_id,a.nomor_po,a.nomor_visual_check,a.tgl_po,d.principle_desc,e.vendor_desc,a.status_po,c.user_name
    from t_po a
    inner join t_po_detail b on a.t_po_id = b.t_po_id
    inner join m_user c on c.user_name = a.careated_by
    left join m_principle d on d.m_principle_id = a.m_principle_id
    left join m_vendor e on e.m_vendor_id = a.m_vendor_id
    where a.t_po_id = '$id'
    group by a.t_po_id,a.nomor_po,a.nomor_visual_check,a.tgl_po,d.principle_desc,e.vendor_desc,a.status_po,c.user_name";
    $dt = mysqli_fetch_array(mysqli_query($con,$sel));
?>
<div class="d-flex flex-row align-items-left" >
    <img src="../../img/chemico.jpeg" style="width:130px;margin-top:-50px; margin-left:20px;height:100px" alt="">
</div>
<b> PT. CHEMICO SURABAYA</b>
<hr>
<div class="form-row col-md-6">
    <div class="col">
        <table>
            <tr>
                <td>Nomor PO</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><?php echo $dt['nomor_po'] ?></td>
            </tr>
            <tr>
                <td>Tgl PO</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><?php echo $dt['tgl_po'] ?></td>
            </tr>
            <tr>
                <td>Supplier</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><?php echo $dt['vendor_desc'] ?></td>
            </tr>
        </table>
    </div>
</div>
<div class="form-row" style="margin-top:25px">
    <table class="table table-bordered">
    <thead>
        <tr>
        <th scope="col">No</th>
        <th scope="col">Kode Barang</th>
        <th scope="col">Nama Barang</th>
        <th scope="col">Jumlah</th>
        <th scope="col">Satuan</th>
        </tr>
    </thead>
    <?php
        $i = 1;
        $sel = "select * from t_po_detail where t_po_id = '$id'";
        $res = mysqli_query($con,$sel);
        while($dt = mysqli_fetch_array($res)){
    ?>    
    <tbody>
        <tr>
        <th scope="row"><?php echo $i; ?></th>
        <td><?php echo $dt['m_barang_id']; ?></td>
        <td><?php echo $dt['nama_barang']; ?></td>
        <td><?php echo $dt['qty']; ?></td>
        <td>Kg</td>
        </tr>
    </tbody>
    <?php
        $i = $i +1;
        }
    ?>  
    </table>
    <div class="form-row">
        <div class="col" style="margin-left:10px">
          Print Date <br>
          <?php
            $currentDateTime = date('Y-m-d');
            echo $currentDateTime;
          ?>
        </div>
    </div>
</div>
</body>