<?php
    $m_user_id  = $_SESSION['user_id'];
?>
<style>

.chosen-container .chosen-results {
    color: #444;
    position: relative;
    overflow-x: hidden;
    overflow-y: auto;
    margin: 0 4px 4px 0;
    padding: 10 10 10 4px;
    max-height: 240px;
   -webkit-overflow-scrolling: touch;
}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $("#kodebarang").change(function(){
            var kode = $("#kodebarang").val();
            $.ajax({
                url:'pages/Transaksi PO/ajaxData.php',
                method : 'post',
                data : 'kode= ' + kode 
            }).done(function(value){
                console.log(value)
            })
        })
</script>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Item PO</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Input PO</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
    <form action="./pages/Transaksi PO/action.php" method="POST" >
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                <input type="hidden" name = "kode_barang" value="<?php echo $kodebarang; ?>" name="randcheck" />

                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <input type="text" placeholder="Nomor PO" onkeyup="refered()" required name="nopo" class="form-control" id="nomorpo" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <input type="date" onchange="referedtgl()"  name="tglpo" required class="form-control" id="tanggalpo" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <select name="vendor" onchange="referedtgl()" required class="form-control" id="povendor">
                                <option value="">- Pilih Vendor -</option>
                                <?php
                                    $varquery = "select * from m_vendor";
                                    $res = mysqli_query($con,$varquery);
                                    while($ds = mysqli_fetch_array($res)){
                                ?>
                                    <option value="<?php echo $ds['m_vendor_id'] ?>"><?php echo $ds['vendor_desc'] ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <select name="principle" hidden required class="form-control" id="sel1">
                                <option value="">- Pilih Principle -</option>
                                <?php
                                    $varquery = "select * from m_principle";
                                    $res = mysqli_query($con,$varquery);
                                    while($ds = mysqli_fetch_array($res)){
                                ?>
                                    <option value="<?php echo $ds['m_principle_id'] ?>"><?php echo $ds['principle_desc'] ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        </form>
        <hr>    
        <form action="./pages/Transaksi PO/action.php" method="POST">
        <div class="form-row">  
            <div class="col col-md-4">
                <div class="form-group">
                        <select name="kodebarang"  class="chosenbarang form-control" id="kodebarang">
                        <option value="">- Barang -</option>
                        <?php
                            $varquery = "select * from m_barang";
                            $res = mysqli_query($con,$varquery);
                            while($ds = mysqli_fetch_array($res)){
                        ?>
                            <option value="<?php echo $ds['m_barang_id'] ?>"><?php echo $ds['nama_barang'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                </div>
            </div>
            <div class="col" style="margin-left:-250px">
                <div class="form-group col-md-8">
                        <input type="text"  name="qty" placeholder="(Kg)" class="form-control" id="exampleInputFirstName" > 
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                     <button class="btn btn-primary" name="detailbtn">Add</button>
                </div>
            </div>
        </div>
        </form>
        <hr>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Item</th>
            <th>QTY</th>
            <th>Satuan</th>
            <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $sel = "select * from t_po_detail_temp a
                inner join m_barang b on a.m_barang_id = b.m_barang_id
                where usercreated = '$m_user_id'";
                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
            ?>
            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['qty']; ?></td>
            <td><?php echo "Kg" ?></td>
            <td style="text-align:center">
                <a href="./pages/Transaksi PO/action.php?act=del_detail&kodebarang=<?php echo $res['m_barang_id']; ?>&created=<?php echo $res['usercreated']; ?>"><button class="btn btn-warning">Hapus</button></a>
            </td>
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
    </div>
    <script>
       function refered(){
         var nopo_data = document.getElementById('nomorpo').value;
         document.getElementById('nopo').value = nopo_data ;
         
       }
       function referedtgl(){
         var nopo_data = document.getElementById('tanggalpo').value;
         var vendor_data = document.getElementById('povendor').value;
         document.getElementById('tglpo').value = nopo_data ;
         document.getElementById('vendor').value = vendor_data ;
       }
    </script>
    <form action="./pages/Transaksi PO/action.php" method="POST">
        <input type="hidden" id="nopo" name="nopo"/>
        <input type="hidden" id="tglpo" name="tglpo"/>
        <input type="hidden" id="vendor" name="vendor"/>
        <div class="col">
            <button class="btn btn-success" name="simpanpo">Simpan PO</button>
        </div>
    </form>
</div>
<script>
    $(".chosenbarang").chosen();
</script>

