<?php
    $where = "where 1=1 and validate_po is null ";
    if(isset($_POST['lihat'])){
      $date1 = $_POST['date1'];
      $date2 = $_POST['date2'];  
      $where = " and tgl_po between '$date1' and '$date2'";
    }
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Purchase Order List</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Input PO</li>
    </ol>
    </div>
    <div>
        <div class="form-row col-md-6">
        <form action="" method="POST">
            <div class="col">
               <div class="form-row ">
                    <div class="col">
                        <input type="date" name="date1" value="<?php echo $date1 ?>" class="form-control" placeholder="First name">
                    </div>
                    <div class="col">
                        <input type="date" name="date2" value="<?php echo $date2; ?>" class="form-control" placeholder="First name">
                    </div>
                    <div class="col">
                        <button class="btn btn-primary" name="lihat">Lihat</button>
                    </div>
               </div><br>
               
            </div>
        </form>
        <div class="col">
            <div class="form-row">
                <div class="col">
                    <a href="./index.php?route=tambahpo"><button class="btn btn-success float-left col-md-4">Buat PO</button></a>
                </div>
            </div>
        </div>
        </div>
        
    </div>
    <div class="card-body">
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No.</th>
                        <th>No. PO</th>
                        <th>Tgl PO</th>
                        <th>Principle</th>
                        <th>Vendor</th>
                        <th>Status</th>
                        <th>Pembuat</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          
                          $sel = "select a.t_po_id,a.nomor_po,a.nomor_visual_check,a.tgl_po,d.principle_desc,e.vendor_desc,a.status_po,c.user_name
                          from t_po a
                          inner join t_po_detail b on a.t_po_id = b.t_po_id
                          inner join m_user c on c.user_name = a.careated_by
                          left join m_principle d on d.m_principle_id = a.m_principle_id
                          left join m_vendor e on e.m_vendor_id = a.m_vendor_id
                          $where
                          group by a.t_po_id,a.nomor_po,a.nomor_visual_check,a.tgl_po,d.principle_desc,e.vendor_desc,a.status_po,c.user_name";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['nomor_po']; ?></td>
                        <td><?php echo $res['tgl_po']; ?></td>
                        <td><?php echo $res['principle_desc']; ?></td>
                        <td><?php echo $res['vendor_desc']; ?></td>
                        <td><?php echo $res['status_po']; ?></td>
                        <td><?php echo $res['user_name']; ?></td>
                        <td style="text-align:center">
                            <!-- <a href="./?route=barang&act=del&id=<?php echo $res['t_po_id']; ?>"></a> -->
                            <button onclick="confirmation('<?php echo $res['t_po_id']; ?>')" class="btn btn-danger">Hapus</button>
                            <a href="./?route=editpo&act=edit&id=<?php echo $res['t_po_id']; ?>"><button class="btn btn-warning">Edit</button></a>
                            <a href="./pages/Transaksi PO/printpo.php?id=<?php echo $res['t_po_id']; ?>"><button class="btn btn-success">Cetak</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
        </div>
    </div>
</div>

<script>
    function confirmation(delName){
    var del=confirm("Yakin Ingin menghapus PO ini..??");
    if (del==true){
        window.location.href="./pages/Transaksi PO/action.php?act=del&id="+delName;
    }
    return del;
}
</script>