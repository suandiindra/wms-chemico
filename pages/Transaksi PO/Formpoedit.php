<?php

    $m_user_id  = $_SESSION['user_id'];
    $act = "";
    $vcek = "";
    $nopo = "";
    $tglpo = "";
    $vendor = "-- Pilih Vendor --";
    $vendroid = "";
    $principleid = "";
    $principle = "";
    $route = "";
    if(isset($_GET['act'])){
        $id = $_GET['id'];
        $caripo= "select a.t_po_id,a.nomor_po,a.nomor_visual_check,a.tgl_po,d.principle_desc
        ,e.m_vendor_id,e.vendor_desc,a.status_po,c.user_name
        ,b.m_barang_id,b.nama_barang,b.qty,a.m_principle_id
        from t_po a
        inner join t_po_detail b on a.t_po_id = b.t_po_id
        inner join m_user c on c.user_name = a.careated_by
        left join m_principle d on d.m_principle_id = a.m_principle_id
        left join m_vendor e on e.m_vendor_id = a.m_vendor_id where a.t_po_id = '$id'";
        $res = mysqli_query($con,$caripo);
        $dt = mysqli_fetch_array($res);
        $vcek = $dt['nomor_visual_check'];
        $nopo = $dt['nomor_po'];
        $tglpo = $dt['tgl_po'];
        $vendroid = $dt['m_vendor_id'];
        $vendor = $dt['vendor_desc'];
        $principleid = $dt['m_principle_id'];
        $principle = $dt['principle_desc'];
    }
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Item PO</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Edit PO</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
    <form action="./pages/Transaksi PO/actionedit.php" method="POST" >
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
                <input type="hidden" name = "id" value="<?php echo $id; ?>" name="randcheck" />

                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <input type="text" value="<?php echo $nopo ?>" placeholder="Nomor PO"  name="nopo" class="form-control" id="exampleInputFirstName" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <input type="date" value="<?php echo $tglpo ?>"  name="tglpo" class="form-control" id="exampleInputFirstName" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <select name="vendor" class="form-control" id="sel1">
                                <option value="<?php echo $vendroid ?>"><?php echo $vendor; ?></option>
                                <?php
                                    $varquery = "select * from m_vendor";
                                    $res = mysqli_query($con,$varquery);
                                    while($ds = mysqli_fetch_array($res)){
                                ?>
                                    <option value="<?php echo $ds['m_vendor_id'] ?>"><?php echo $ds['vendor_desc'] ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <select name="principle" required class="form-control" id="sel1">
                                <option value="<?php $principleid ?>"><?php echo $principle; ?></option>
                                <?php
                                    $varquery = "select * from m_principle";
                                    $res = mysqli_query($con,$varquery);
                                    while($ds = mysqli_fetch_array($res)){
                                ?>
                                    <option value="<?php echo $ds['m_principle_id'] ?>"><?php echo $ds['principle_desc'] ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                               <button class="btn btn-success" name="simpanpo">Edit PO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <hr>    
        <form action="./pages/Transaksi PO/actionedit.php" method="POST">
        <?php
            $rand=rand();
            $_SESSION['rand']=$rand;
        ?>  
        <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
        <input type="hidden" value="<?php echo $_GET['id']; ?>" name="id" />
        <div class="form-row md-col-12">  
            <div class="col">
                <div class="form-group">
                        <select name="kodebarang" class="form-control" id="sel1">
                        <option value="">- Barang -</option>
                        <?php
                            $varquery = "select * from m_barang";
                            $res = mysqli_query($con,$varquery);
                            while($ds = mysqli_fetch_array($res)){
                        ?>
                            <option value="<?php echo $ds['m_barang_id'] ?>"><?php echo $ds['nama_barang'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                        <input type="text"  name="qty" placeholder="(Kg)" class="form-control" id="exampleInputFirstName" > 
                        <input type="hidden"  name="paramback" value="<? $parambak ?>" class="form-control" id="exampleInputFirstName" >
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                     <BUTTon class="btn btn-primary" name="detailbtn">Add</BUTTon>
                </div>
            </div>
        </div>
        </form>
        <hr>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Item</th>
            <th>QTY</th>
            <th>Satuan</th>
            <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if(isset($_GET['act'])){
                $sel = "select * from t_po_detail where t_po_id = '$id'";
            }else{
                $sel = "select * from t_po_detail_temp a
                inner join m_barang b on a.m_barang_id = b.m_barang_id
                where usercreated = '$m_user_id'";
            }
                
                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
            ?>
            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['qty']; ?></td>
            <td><?php echo "Kg" ?></td>
            <td style="text-align:center">
                 <a href="./pages/Transaksi PO/actionedit.php?act=del_exsist&kodebarang=<?php echo $res['m_barang_id']; ?>&id=<?php echo $_GET['id']; ?>"><button class="btn btn-warning">Hapus</button></a>
            </td>
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
    </div>
</div>