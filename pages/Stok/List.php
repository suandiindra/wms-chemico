<?php
    $where = " where 1 = 1";
    if(isset($_POST['lihatmutasi'])){
      $date1 = $_POST['date1'];
      $date2 = $_POST['date2'];  
      $cust  = $_POST['cust'];
      
      $where = $where." and tgl_do between '$date1' and '$date2'";
      if($cust){
        $where = $where. " and a.m_customer_id = '$cust'";
      }
    }
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Lap Stok Gudang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Stok</li>
    </ol>
</div>
    <div class="card-body" style="margin-top:-30px">
        <div class="col col-md-2" style="margin-left:-10px">
            <a href="./pages/Stok/export_stok.php"><button class="btn btn-success" style="margin-top:10px" name="lihat">Export Data</button></a>
        </div>
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No.</th>
                        <th>Nama Barang</th>
                        <th>Principle</th>
                        <th>Batch</th>
                        <th>Expired</th>
                        <!-- <th>Gudang</th> -->
                        <th>Rak</th>
                        <th>Stok Awal</th>
                        <th>Masuk</th>
                        <th>Keluar</th>
                        <th>Adj Masuk</th>
                        <th>Adj Keluar</th>
                        <th>Stok Akhir</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          
                          $sel = "select t_stok_id,b.nama_barang,c.principle_desc,a.batch,a.expired,gudang_desc,rak_desc,qty_masuk,qty_keluar
                          ,qty_adj_masuk,qty_adj_keluar,last_stok,stok_awal
                          from t_stok a
                          inner join m_barang b on a.m_barang_id = b.m_barang_id
                          inner join m_principle c on c.m_principle_id = b.m_principle_id
                          inner join m_rak d on d.m_rak_id = a.m_rak_id
                          inner join m_gudang e on e.m_gudang_id = d.m_gudang_id $where
                          order by a.m_barang_id";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['nama_barang']; ?></td>
                        <td><?php echo $res['principle_desc']; ?></td>
                        <td><?php echo $res['batch']; ?></td>
                        <td><?php echo $res['expired']; ?></td>
                        <!-- <td><?php echo $res['gudang_desc']; ?></td> -->
                        <td><?php echo $res['rak_desc']; ?></td>
                        <td><?php echo format($res['stok_awal']); ?></td>
                        <td><?php echo format($res['qty_masuk']); ?></td>
                        <td><?php echo format($res['qty_keluar']); ?></td>
                        <td><?php echo format($res['qty_adj_masuk']); ?></td>
                        <td><?php echo format($res['qty_adj_keluar']); ?></td>
                        <td><?php echo format($res['last_stok']); ?></td>
                        <td style="text-align:center">
                           <a href="./?route=stokmap&id=<?php echo $res['t_stok_id']; ?>"><button class="btn btn-success">Lihat</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
        </div>
    </div>
</div>

<script>
    function confirmation(delName){
    var del=confirm("Yakin Ingin menghapus PO ini..??");
    if (del==true){
        window.location.href="./pages/Transaksi PO/action.php?act=del&id="+delName;
    }
    return del;
}
</script>