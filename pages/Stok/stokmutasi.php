<?php
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $cekstok = "select * from t_stok where t_stok_id = '$id'";
        $dt = mysqli_fetch_array(mysqli_query($con,$cekstok));
        $m_barang_id = $dt['m_barang_id'];
        $batch = $dt['batch'];
        $m_rak_id = $dt['m_rak_id'];
        $qr = "
        select * from (
        select a.t_gr_id as id,tipe_gr as ket,'IN' as jenis,a.tgl_gr as periode,c.nama_barang,principle_desc
        ,batch,rak_desc,b.qty_gr  as jml ,'' tujuan,'background-color:#7FFFD4; color:black' as color,a.penerima as usercreated from t_gr a
        inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        where b.m_barang_id = '$m_barang_id' and batch = '$batch' and b.m_rak_id = $m_rak_id
        union
        select a.t_do_id as id,tipe_do as ket,'OUT' as jenis,a.tgl_do,c.nama_barang,principle_desc
        ,batch,rak_desc,b.qty  as jml,f.customer_desc tujuan,'background-color:#DC143C; color:white' as color,a.usercreated as usercreated from t_do a
        inner join t_do_detail b on a.t_do_id = b.t_do_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        left join m_customer f on f.m_customer_id = a.m_customer_id
        where b.m_barang_id = '$m_barang_id' and batch = '$batch' and b.m_rak_id = $m_rak_id
        union
        select a.t_mutasi_id as id,'Transfer Keluar' as ket,'OUT' as jenis,a.tgl_mutasi as periode,c.nama_barang,principle_desc
        ,batch,e.rak_desc,b.jumlah  as jml,f.rak_desc tujuan,'background-color:#B22222; color:white' as color,a.usercreated as usercreated  from t_mutasi a
        inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id_source
        inner join m_rak f on f.m_rak_id = b.m_rak_id_dest
        where b.m_barang_id = '$m_barang_id' and batch = '$batch' and b.m_rak_id_source = $m_rak_id
        union
        select a.t_mutasi_id as id,'Transfer Masuk' as ket,'IN' as jenis,a.tgl_mutasi as periode,c.nama_barang,principle_desc
        ,batch,e.rak_desc,b.jumlah  as jml,f.rak_desc as tujuan,'background-color:#228B22; color:white' as color,a.usercreated as usercreated  from t_mutasi a
        inner join t_mutasi_detail b on a.t_mutasi_id = b.t_mutasi_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id_dest
        inner join m_rak f on f.m_rak_id = b.m_rak_id_source
        where b.m_barang_id = '$m_barang_id' and batch = '$batch' and b.m_rak_id_dest = $m_rak_id
        union
        select a.t_adjustment_id as id,'Adjustment Masuk' as ket,'IN' as jenis,a.tgl_adjustment as periode,c.nama_barang
        ,principle_desc ,batch,rak_desc,b.qty as jml,'' tujuan,'background-color:#20B2AA; color:white' as color,a.usercreated as usercreated from t_adjustment a 
        inner join t_adjustment_detail b on a.t_adjustment_id = b.t_adjustment_id and b.eksekusi = 'PLUS'
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id 
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        where b.m_barang_id = '$m_barang_id' and batch = '$batch' and b.m_rak_id = $m_rak_id
        union
        select a.t_adjustment_id as id,'Adjustment Keluar' as ket,'OUT' as jenis,a.tgl_adjustment as periode,c.nama_barang
        ,principle_desc ,batch,rak_desc,b.qty as jml,'' tujuan,'background-color:#FF4500; color:white' as color,a.usercreated as usercreated from t_adjustment a 
        inner join t_adjustment_detail b on a.t_adjustment_id = b.t_adjustment_id and b.eksekusi = 'MINUS'
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id 
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        where b.m_barang_id = '$m_barang_id' and batch = '$batch' and b.m_rak_id = $m_rak_id
        union
        select a.t_retur_id as id,'Barang Retur' as ket,'OUT' as jenis,a.tgl_retur,c.nama_barang,principle_desc
        ,batch,rak_desc,b.qty  as jml,f.vendor_desc tujuan,'background-color:#FF6347; color:white' as color,a.usercreated as usercreated   from t_retur a
        inner join t_retur_detail b on a.t_retur_id = b.t_retur_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id 
        inner join m_principle d on d.m_principle_id = c.m_principle_id
        inner join m_rak e on e.m_rak_id = b.m_rak_id
        left join m_vendor f on f.m_vendor_id = a.m_vendor_id
        where b.m_barang_id = '$m_barang_id' and batch = '$batch' and b.m_rak_id = $m_rak_id
        ) data order by periode";

        // echo $qr;
        // console($qr);
        $cek_stok = "select * from t_stok where m_barang_id = '$m_barang_id' and batch = '$batch' and m_rak_id = '$m_rak_id'";
        $resx = mysqli_query($con,$cek_stok);
        $dtc = mysqli_fetch_array($resx);
        $stok_awal = $dtc['stok_awal'];

        $res = mysqli_query($con,$qr);
    }
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Historical Stok</h1><br>
    
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Edit PO</li>
    </ol>
    </div>
    <div style="float:right; margin-right:30px; margin-top:10px">
      <h4 style="color:red">  STOK AWAL = <?php echo $stok_awal ?></h4>
    </div>
    <hr>
    <div class="card-body">
    <span class="glyphicon glyphicon-user" style="background-color:#7FFFD4">&nbsp&nbsp&nbsp&nbsp</span> Receiptment |
    <span class="glyphicon glyphicon-user" style="background-color:#DC143C">&nbsp&nbsp&nbsp&nbsp</span> Delivery Order |
    <span class="glyphicon glyphicon-user" style="background-color:#B22222">&nbsp&nbsp&nbsp&nbsp</span> Transfer Rak Out |
    <span class="glyphicon glyphicon-user" style="background-color:#228B22">&nbsp&nbsp&nbsp&nbsp</span> Transfer rak IN |
    <span class="glyphicon glyphicon-user" style="background-color:#20B2AA">&nbsp&nbsp&nbsp&nbsp</span> ADJ Masuk |
    <span class="glyphicon glyphicon-user" style="background-color:#FF4500">&nbsp&nbsp&nbsp&nbsp</span> ADJ Keluar |
    <span class="glyphicon glyphicon-user" style="background-color:#FF6347">&nbsp&nbsp&nbsp&nbsp</span> Retur 
    <form action="" method="POST">
        <div class="row">
            <div class="col-lg-8">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
            </div>
        </div>
        </form>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Keterangan</th>
            <th>IN/OUT</th>
            <th>Periode</th>
            <th>Nama Barang</th>
            <th>Principle</th>
            <th>Tujuan</th>
            <th>Batch</th>
            <th>Rak</th>
            <th>PIC</th>
            <th>Jumlah</th>
            <th>Balance</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $result = mysqli_query($con,$qr);
                $i = 1;
                
                while($res = mysqli_fetch_array($result)){
                $color = $res['color'];
                $jml = $res['jml'];
                if($res['jenis'] == "OUT"){
                    $jml = "-".$res['jml'];
                }
                $stok_awal = $stok_awal + $jml;
            ?>
            
            <tr style="<?php echo $color; ?>">
            <td><?php echo $i; ?></td>
            <td><?php echo $res['ket']; ?></td>
            <td><?php echo $res['jenis']; ?></td>
            <td><?php echo $res['periode']; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['principle_desc']; ?></td>
            <td><?php echo $res['tujuan']; ?></td>
            <td><?php echo $res['batch']; ?></td>
            <td><?php echo $res['rak_desc']; ?></td>
            <td><?php echo $res['usercreated']; ?></td>
            <td><?php echo $jml; ?></td>
            <td><?php echo $stok_awal; ?></td>
            </tr>
            <?php
                
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
    </div>
    <button class="btn btn-success" onclick="goBack()">Kembali</button>
    <script>
    function goBack() {
        window.history.back();
    }
    </script>
</div>