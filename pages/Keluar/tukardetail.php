<?php
    $m_user_id  = $_SESSION['user_id'];
    $t_do_id = $_GET['id'];

    if(isset($_POST['vcval'])){
        if(isset($_POST['randcheck'])){
            $id = $_POST['idval'];
            $val = $_POST['vcval'];
            $q = "update t_do set catatan = '$val' where t_do_id = '$id'";
            mysqli_query($con,$q);
        }
    }
?>

<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Delivery Order Details</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Input PO</li>
    </ol>
    </div>
    <?php
        $s = mysqli_query($con,"select * from t_do a inner join m_customer b on a.m_customer_id = b.m_customer_id 
        where t_do_id = '$t_do_id'");
        $da = mysqli_fetch_array($s);
    ?>
    <script>
        function ref(){
            var n = document.getElementById("vcek").value;
            document.getElementById("vcval").value = n;
        }
    </script>
    <div class="col">
        <table>
            <tr>
                <td>Tanggal DO</td>
                <td style="padding-left:20px;padding-right:20px">:</td>
                <td><?php echo $da['tgl_do']; ?></td>
            </tr>
            <tr>
                <td>Customer</td>
                <td style="padding-left:20px;padding-right:20px">:</td>
                <td><?php echo $da['customer_desc']; ?></td>
            </tr>
            <tr>
                <td>Catatan</td>
                <td style="padding-left:20px;padding-right:20px">:</td>
                <td><?php echo $da['reference_key']; ?></td>
            </tr>
            <tr>
                <td>Nomor V-Check</td>
                <td style="padding-left:20px;padding-right:20px">:</td>
            <?php
                if($da['catatan']){
            ?>
                 <td><?php echo $da['catatan']; ?></td>
            <?php
                }else{
            ?>
                 <td><input type="text" id="vcek" onkeyup="ref()" name="vc" /></td>
            <?php
                }
            ?>
               
            </tr>
            <tr>
            <?php
                if(!$da['catatan']){
            ?>
             <td colspan="3" >
             <Form action="./?route=tukardetail&id=<?php echo $_GET['id'] ?>" method="POST">
             <?php
                $rand=rand();
                $_SESSION['rand']=$rand;
            ?>  
            <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
             <input type="hidden" id="vcval" name="vcval" />
             <input type="hidden"  name="idval" value="<?php echo $_GET['id'] ?>" />
                <Button class="btn btn-primary col-md-3">Edit</button>
            </form>
             </td>

            <?php
                }
            ?>
            </tr>
        </table>
    </div>
    <hr>
    <div class="card-body">
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Item</th>
            <th>Batch</th>
            <th>Gudang Rak</th>
            <th>QTY</th>
            <th>Satuan</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $sel = "select * from t_do_detail a
                left join t_stok b on a.m_barang_id = b.m_barang_id and a.batch = b.batch and a.m_rak_id = b.m_rak_id
                left join m_barang c on c.m_barang_id = a.m_barang_id
                left join m_rak d on d.m_rak_id = b.m_rak_id
                where a.t_do_id = '$t_do_id'";

                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
            ?>
            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['batch']; ?></td>
            <td><?php echo $res['rak_desc']; ?></td>
            <td><?php echo $res['qty']; ?></td>
            <td><?php echo "Kg" ?></td>
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
    </div>
</div>
<script>
    $(".chosenbarang").chosen();
</script>