<?php
    $where = "";
    if(isset($_POST['lihatdo'])){
      $date1 = $_POST['date1'];
      $date2 = $_POST['date2'];  
      $cust  = $_POST['cust'];
      $tipe = $_POST['tipe'];
      $where = " where 1=1";
     
      if($cust){
        $where = $where. " and a.m_customer_id = '$cust'";
      }

      if($tipe){
        $where = $where. " and a.tipe_do = '$tipe' ";
      }
      if($date1){
        $where = $where." and tgl_do between '$date1' and '$date2'";
      }
    }
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Delivery Order List</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Delivery Order</li>
    </ol>
    </div>
    <div>
        <div class="form-row ">
        <form action="" method="POST">
            <div class="col">
               <div class="form-row col-md-12">
                    <div class="col col-md-2">
                        <input type="date" name="date1" value="<?php echo $date1 ?>" class="form-control" placeholder="First name">
                    </div>
                    <div class="col col-md-2">
                        <input type="date" name="date2" value="<?php echo $date2; ?>" class="form-control" placeholder="First name">
                    </div>
                    <div class="col col-md-3">
                        <select name="cust" id="" class="pilih">
                            <option value="">-- Semua --</option>
                            <?php
                                $s = mysqli_query($con,"select * from m_customer");
                                while($dt = mysqli_fetch_array($s)){
                                    $id = $dt['m_customer_id'];
                            ?>
                                <option value="<?php echo $dt['m_customer_id'] ?>"> <?php  echo $dt['customer_desc']; ?></option>
                            <?php
                                }
                                
                            ?>
                        </select>
                    </div>
                    <div class="col col-md-2" style="margin-left:0px">
                        <select name="tipe" id="" class="form-control">
                            <option value="">-- Semua --</option>
                            <option value="DO Customer">DO Customer</option>
                            <option value="Tukar DO">Tukar DO</option>
                        </select>
                    </div>
                    <div class="col col-md-2">
                        <button class="btn btn-primary" name="lihatdo">Lihat</button>
                    </div>
               </div>
               <br>
            </div>
        </form>
        <br>
        <div class="col">
            <div class="form-row">
                <div class="col">
                    <a href="./index.php?route=tambahdo"><button class="btn btn-success float-left col-md-2">Buat Doc</button></a>
                    <a href="./pages/Keluar/export_do.php"><button class="btn btn-danger col-md-2">Export</button></a>
                </div>
            </div>
        </div>
        </div>
        
    </div>
    <div class="card-body">
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No.</th>
                        <th>No. DO</th>
                        <th>Tgl DO</th>
                        <th>Tipe DO</th>
                        <th>Customer</th>
                        <th>QTY</th>
                        <th>Dibuat Oleh</th>
                        <th>Catatan</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          
                          $sel = "select a.t_do_id,a.nomor_do,tgl_do,e.customer_desc,tipe_do,reference_key,sum(cast(b.qty as float))qty,a.usercreated from t_do a
                          inner join t_do_detail b on a.t_do_id = b.t_do_id
                          left join m_barang c on c.m_barang_id = b.m_barang_id
                          inner join t_stok d on d.m_barang_id = b.m_barang_id and d.batch = d.batch and b.m_rak_id = d.m_rak_id
                          left join m_customer e on e.m_customer_id = a.m_customer_id $where
                          group by a.t_do_id,a.nomor_do,tgl_do,e.customer_desc,a.usercreated,tipe_do,reference_key";

                        //   echo $sel;
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['nomor_do']; ?></td>
                        <td><?php echo $res['tgl_do']; ?></td>
                        <td><?php echo $res['tipe_do']; ?></td>
                        <td><?php echo $res['customer_desc']; ?></td>
                        <td><?php echo format($res['qty'],2); ?></td>
                        <td><?php echo $res['usercreated']; ?></td>
                        <td><?php echo $res['reference_key']; ?></td>
                        <td style="text-align:center">
                           <a href="./?route=keluardetail&id=<?php echo $res['t_do_id']; ?>"><button class="btn btn-success">Lihat</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
        </div>
    </div>
</div>

<script>
    $(".pilih").chosen();
    function confirmation(delName){
    var del=confirm("Yakin Ingin menghapus PO ini..??");
    if (del==true){
        window.location.href="./pages/Transaksi PO/action.php?act=del&id="+delName;
    }
    return del;
}
</script>