<?php
    $m_user_id  = $_SESSION['user_id'];
?>
<script type="text/javascript">

    
    $(document).ready(function(){
        $("#partialdo").load("pages/Keluar/ajaxLoad.php",{
        
        });


        $("#detailbtn").click(function(){
            var kode = document.getElementById("kode").value;
            var batch = document.getElementById("batch").value;
            var rak_ = document.getElementById("rak_").value;
            var jml  = document.getElementById("jml").value;
            var ck  = document.getElementById("cx");
            var stok = document.getElementById("stok").innerHTML;
            if (ck.checked == true){
                ck = true
                jml = stok.replace("Stok = ","");
                console.log(jml)
            }else{
                ck = false
            }                    
            $("#partialdo").load("pages/Keluar/ajaxLoad.php",{
                kode : kode,
                batch : batch,
                rak : rak_,
                jml : jml,
                all : ck
            })
            if(kode.length == 0 || batch.length == 0 || rak_.length == 0 || jml.length == 0){
                $("#err").text("Lengkapi isian yang benar !!!");
            }else{
                $("#err").text("");
                document.getElementById("kode").value = "";
                document.getElementById("batch").value = "";
                document.getElementById("rak_").value = "";
                document.getElementById("jml").value = "";
                document.getElementById("stok").value = "";
            }
            console.log(kode,batch,rak_,jml,ck)
        })

        $("#batch").change(function(){
            var kode = $("#batch").val();
            var barang = document.getElementById("kode").value;
            // console.log(kode);
            $.ajax({
                url:'pages/Keluar/ajaxData.php',
                method : 'post',
                data : 'kode_batch= ' + kode +'&barang= '+barang
            }).done(function(vl){
                var value = vl.split("#")
                console.log(value[0])
                $('#rak_').html(value[1]);
                $("#stok").text('');
                // $('#rak').val(vl);
            })
        })
        
        $("#rak_").change(function(){
            var rak = $("#rak_").val();
            var m_barang = document.getElementById("kode").value;
            var batch = document.getElementById("batch").value;
            // console.log(kode);
            $.ajax({
                url:'pages/Keluar/ajaxData.php',
                method : 'post',
                data : 'rak= ' + rak +"&barang= "+m_barang+"&batch="+batch
            }).done(function(vl){
                var value = vl.split("#")
                console.log(value[0])
                $("#stok").text('Stok = '+value[1]);
            })
        })
    })
</script>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Delivery Order Form</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Input PO</li>
    </ol>
    </div>
    <hr>
    <div class="card-body">
    <form action="./pages/Keluar/actiondo.php" method="POST" >
        <div class="row">
            <div class="col-lg-8">
                <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
                ?>  
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />

                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <input type="text" required placeholder="Nomor DO" onkeyup="refered()" name="nodo" class="form-control" id="nodo" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <input type="date" required  name="tgldo" onchange="referedtgl()" required class="form-control" id="tgldo" >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <select name="tip_edo" onchange="referedtgl()" required class="form-control" id="tipe_do" >
                                <option value="DO Customer">- Tipe DO -</option>
                                <option value="DO Customer">DO Customer</option>
                                <option value="Tukar DO">Tukar DO</option>
                                </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <select name="customer" onchange="referedtgl()" required class="form-control" id="customer">
                                <option value="">- Pilih Cusmtomer -</option>
                                <?php
                                    $varquery = "select * from m_customer";
                                    $res = mysqli_query($con,$varquery);
                                    while($ds = mysqli_fetch_array($res)){
                                ?>
                                    <option value="<?php echo $ds['m_customer_id'] ?>"><?php echo $ds['customer_desc'] ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                        </div>
                    </div>
                    
                   
                </div>
            </div>
        </div>
        </form>
        <hr>    
        <!-- <form action="./pages/Keluar/actiondo.php" method="POST"> -->
        <div class="form-row md-col-12">  
            <div class="col">
                <div class="form-group">
                    <select name="kode" id="kode" required class="chosenbarang">
                    <option value="">-- Pilih Barang --</option>
                    <?php
                        $s = "select a.m_barang_id,nama_barang from t_stok a
                        inner join m_barang b on a.m_barang_id = b.m_barang_id
                        group by a.m_barang_id,nama_barang;";
                        $res = mysqli_query($con,$s);
                        while($dt = mysqli_fetch_array($res)){
                    ?>
                        <option value="<?php echo $dt['m_barang_id'] ?>"><?php echo $dt['nama_barang']; ?></option>
                    <?php
                        }
                    ?>
                </select>
                </div>
                <u><span id="prc1" style="color:red;font-size:12px; margin-top:-10px"></span></u>
            </div>
            <div class="col">
                <div class="form-group">
                <select name="batch" id="batch" required class="form-control">
                    <option value="">-- Pilih Batch --</option>
                </select>
                </div>
            </div>
            <!-- <div class="col">
                <div class="form-group">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Default checkbox
                </label>
                </div>
            </div> -->
            <div class="col">
                <div class="form-group">
                <select name="rak_" id="rak_" required class="form-control">
                    <option value="">-- Pilih Rak --</option>
                </select>
                </div>
                <b><span id="stok" style="color:blue;font-size:12px; margin-top:-10px"></span></b>
            </div>
            <div class="col">
                <div class="form-group">
                        <input type="text"  name="jml" id="jml" required placeholder="(Kg)" class="form-control" id="exampleInputFirstName" > 
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                     <BUTTon class="btn btn-primary" name="detailbtn" id="detailbtn">Add</BUTTon>
                     <span id="err" style="margin-top:110px; color:red"></span>
                </div>
                <div class="form-check">
                <input class="form-check-input" name="cx" id="cx" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label primary" for="defaultCheck1">
                    Ambil Semua
                </label>
                </div>
            </div>
            <!-- <div class="col">
                <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Default checkbox
                </label>
                </div>
            </div> -->
        </div>
        <!-- </form> -->
        <hr>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Item</th>
            <th>Batch</th>
            <th>Gudang Rak</th>
            <th>QTY</th>
            <th>Satuan</th>
            <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody id="partialdo">
      
        </tbody>
        </table>
        <form action="./pages/Keluar/actiondo.php" method="POST">
            <script>
            function refered(){
                var nopo_data = document.getElementById('nodo').value;
                document.getElementById('nomordo').value = nopo_data ;
                
            }
            function referedtgl(){
                var nopo_data = document.getElementById('tgldo').value;
                var customer = document.getElementById('customer').value;
                var tipedo = document.getElementById('tipe_do').value;
                document.getElementById('tanggaldo').value = nopo_data ;
                document.getElementById('customerdo').value = customer ;
                document.getElementById('tipedo').value = tipedo ;
            }
            </script>
            <input type="hidden" name="nodo" id="nomordo">
            <input type="hidden" name="tgldo" id="tanggaldo">
            <input type="hidden" name="customer" id="customerdo">
            <input type="hidden" name="tipedo" id="tipedo">
            <input type="text" name="idref" class="form-control col-md-2" placeholder="Nomor V-CHECK"><br>
            <div class="col col-md-3"  style="margin-left:-15px">
                <textarea class="form-control" name="ref" id="ref" placeholder="Catatan DO"></textarea>
            </div>
            <br>
            <div class="col" style="margin-left:-15px">
                <button class="btn btn-success" name="simpando">Proses DO</button>
            </div>
        </form>
        
    </div>
</div>
<script>
    $(".chosenbarang").chosen();
    $('#kode').on('change', function(e) {
        var kode = $("#kode").val();
        console.log(kode);
        $.ajax({
            url:'pages/Keluar/ajaxData.php',
            method : 'post',
            data : 'kode= ' + kode 
        }).done(function(value){
            var value = value.split("#")
            $('#batch').html(value[0]);
            // console.log(value[1])
            $("#prc1").text(value[1]);
            $("#stok").text('');
        })
    })

    $("#customer").chosen();
</script>