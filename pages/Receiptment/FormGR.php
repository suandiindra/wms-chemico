<script type="text/javascript">
    $(document).ready(function(){
        $("#kode").change(function(){
            var kode = $("#kode").val();
            // console.log(kode);
            $.ajax({
                url:'pages/Receiptment/ajaxData.php',
                method : 'post',
                data : 'kode= ' + kode 
            }).done(function(value){
                console.log(value)
                $('#gudangrak').html(value);
            })
        })

        $("#jenis").change(function(){
            var kode = $("#jenis").val();
            // console.log(kode);
            $.ajax({
                url:'pages/Receiptment/ajaxData.php',
                method : 'post',
                data : 'jenispo='+ kode 
            }).done(function(value){
                console.log(value)
                $('#vendor').html(value);
            })
        })

        // $("#partial").remove();
        $("#partial").load("pages/Receiptment/ajaxload.php",{

        })
        
        $("#addgr").click(function(){
            // $("#partial").remove();
            var kodebarang_ =  document.getElementById("kodebrg").value;
            var batch_ =  document.getElementById("batch").value;
            var expired_ =  document.getElementById("expired").value;
            var nopack_ =  document.getElementById("nopack").value;
            var size_ =  document.getElementById("size").value;
            var qty_ =  document.getElementById("qty").value;
            var kode_ =  document.getElementById("kode").value;
            var gudangrak_ =  document.getElementById("gudangrak").value;
            var notes_ =  document.getElementById("notes").value;
            
            $("#partial").load("pages/Receiptment/ajaxload.php",{
                kodebarang : kodebarang_,
                expired : expired_,
                nopack : nopack_,
                packsize : size_,
                batch : batch_,
                gudangrak : gudangrak_,
                nopack : nopack_,
                qty : qty_,
                catatan : notes_

            })
            if(kodebarang_.length == 0 || batch_.length == 0 || qty_.length == 0 || gudangrak_.length == 0){
                $("#err").text("Lengkapi isian yang benar !!!");
                console.log("gabener")
            }else{
                $("#err").text("")
                $("#prc").text("")
                document.getElementById("kodebrg").value = "";
                document.getElementById("batch").value = "";
                document.getElementById("expired").value = "";
                document.getElementById("nopack").value = "";
                document.getElementById("size").value = "";
                document.getElementById("qty").value = "";
                document.getElementById("kode").value = "";
                document.getElementById("gudangrak").value = "";
                document.getElementById("notes").value = "";
            }
            
            // console.log(kodebarang,batch,expired,nopack,size,qty);
        })
    })
</script>
<?php
    $ponomer = nomorGR($con,"");
    $m_user_id  = $_SESSION['user_id'];
    $t_po_id = "";
    $act = "";
    $novceck  = "";
    $nopo = "";
    $tglpo = "";
    $vendor = "";
    $created = "";
    $grstatus = "";
    
    if(isset($_POST['konfirmasi'])){
        $po = $_POST['po'];
        $tglpo = $_POST['potgl'];
        $vnomer = $_POST['vnomer'];
        $povendor = $_POST['povendor'];
        $principle = $_POST['poprinciple'];
        $driver = $_POST['driver'];
        $plat = $_POST['plat'];
        $jenispo = $_POST['jenispo'];
        $catatan = $_POST['catatan'];
        $uniq = uniq();
        $vnomer = nomorGR($con,$jenispo);
        if($jenispo == ""){
            echo "<script>alert('Lengkapi isian');</script>";
            echo "<script>window.location='./?route=tambahgr'</script>";
            // echo "window.location='./'";
            // exit;
        }else{
            $selx = "select * from t_gr_detail_temp where usercreated = '$m_user_id'";
            // echo $selx;
            $resp = mysqli_query($con,$selx);
            $cek = mysqli_num_rows($resp);
            if($cek == 0){
                echo "<script>alert('Isi item dulu....')</script>";
                // echo "<script>window.location='./?route=tambahgr'</script>";
            }else{
                $ins = "insert into t_po (t_po_id,nomor_po,nomor_visual_check,m_vendor_id
                ,m_principle_id,tgl_po,status_po,careated_by,created_date,validate_po) values 
                ('$uniq','$po','$vnomer','$povendor','$principle','$tglpo','COMPLETE','$m_user_id',now(),now())";

                $ins_detail = " insert into t_po_detail (t_po_detail_id,t_po_id,m_barang_id,nama_barang,qty,created_date) 
                                select '$uniq','$uniq',a.m_barang_id,nama_barang,sum(qty),now() from t_gr_detail_temp a
                                inner join m_barang b on a.m_barang_id = b.m_barang_id
                                group by a.m_barang_id,nama_barang";
                
                $uniqGR = uniq();
                $insgr = "insert into t_gr
                        select '$uniqGR','$tglpo','$uniq','$vnomer','1','$driver','$m_user_id','$plat',now(),'$jenispo','$catatan'";
                $insgr_detail = "insert into t_gr_detail (t_gr_id,m_barang_id,qty_gr,batch,nopack,packsize
                ,expired,m_rak_id,remark,createdate,stok_awal)
                select '$uniqGR',m_barang_id,qty,batch,nopack,packsize,expired,m_rak_id,remark,now(),stok from t_gr_detail_temp
                where usercreated = '$m_user_id'";

                // echo $insgr_detail;
                $deltemp = "delete from t_gr_detail_temp where usercreated = '$m_user_id'";
                mysqli_query($con,$ins);
                mysqli_query($con,$ins_detail);
                mysqli_query($con,$insgr);
                mysqli_query($con,$insgr_detail);
                mysqli_query($con,$deltemp);

                $cekpilih = mysqli_query($con,"select * from t_gr_detail where t_gr_id = '$uniqGR'");
                while($dt = mysqli_fetch_array($cekpilih)){
                    $m_barang_id  = $dt['m_barang_id'];
                    $batch  = $dt['batch'];
                    $rak  = $dt['m_rak_id'];
                    $expired = $dt['expired'];
                    $jml = $dt['qty_gr'];
                    $cek = "select * from t_stok where m_barang_id = '$m_barang_id' and batch = '$batch' and m_rak_id = '$rak' limit 1";
                    // echo $cek."</br>";
                    $rsdt = mysqli_query($con,$cek);
                    $row = mysqli_num_rows($rsdt);
                    if($row > 0){
                        $ro = mysqli_fetch_array($rsdt);
                        $stok_id = $ro['t_stok_id'];
                        $res = mysqli_query($con,"update t_stok set qty_masuk = qty_masuk + $jml, last_stok = last_stok + $jml 
                        where t_stok_id = '$stok_id'");
                        // echo "update t_stok set qty_masuk = qty_masuk + $jml, last_stok = last_stok + $jml 
                        // where t_stok_id = '$stok_id'";
                    }else{
                        $stok_id = uniq();
                        $res = mysqli_query($con,"insert into t_stok 
                        (m_barang_id,batch,expired,m_rak_id,stok_awal,qty_masuk,qty_adj_masuk,qty_keluar,qty_adj_keluar,last_stok,last_update)
                        select '$m_barang_id','$batch','$expired','$rak','0','$jml','0','0','0','$jml',now()");
                        // echo "insert into t_stok 
                        // select '$stok_id','$m_barang_id','$batch','$expired','$rak','0','$jml','0','0','0','$jml',now()";
                    }
                }
                echo "<script>window.location='./?route=GR'</script>";
            }
            
        }
    }

    if(isset($_GET['act'])){
        $id = $_GET['id'];
        $caripo= "delete from t_gr_detail_temp where t_gr_detail_temp_id = '$id'";

        // echo $caripo;
        $res = mysqli_query($con,$caripo);
        echo "<script>window.location='./?route=tambahgr'</script>";
    }
    if(isset($_POST['trfbarang'])){
        if(isset($_POST['randcheck'])){
            $kodebarang = $_POST['kodebarang'];
            $expired = $_POST['expired'];
            $nopack= $_POST['nopack'];
            $packsize= $_POST['packsize'];
            $qty= $_POST['qty'];
            $gudangrak= $_POST['gudangrak'];
            $batch= $_POST['batch'];
            $catatan=$_POST['catatan'];
            $temp_id = uniq();
            $ins = "INSERT into t_gr_detail_temp (t_gr_detail_temp_id,m_barang_id, qty,batch,m_rak_id,expired,nopack,packsize,remark,usercreated)
            values ('$temp_id','$kodebarang','$qty','$batch','$gudangrak','$expired','$nopack','$packsize','$catatan','$m_user_id')";

            $res = mysqli_query($con,$ins);
        }
    }
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    </div>
    <div class="card-body">
    <form action="" method="POST">
        <div class="row">
            <div class="col-lg-12" >
                <h2>Header Form Penerimaan Barang</h2>
                * Nomor Virtual Check akan otomatis terbuat secara sistem
                <div class="form-row">
                   
                    <div class="col  col-md-2">
                        <div class="form-group">
                            <input type="text" onkeyup="refered()"  placeholder="Masukan Nomor PO" name="nopo" id = "nopo" required class="form-control" >
                        </div>
                    </div>

                    <!-- <div class="col col-md-2">
                        <div class="form-group">
                            <input type="text" id = "vno" readonly onkeyup="refered()" value="<?php echo $ponomer ?>" placeholder="Virtual Check Nomor" required name="novceck" class="form-control" >
                        </div>
                    </div> -->
                    <div class="col col-md-2">
                        <div class="form-group">
                            <input type="date" name="tglpo" id="tglpo" onchange="referedtgl()" value="<?php echo $novceck ?>"  required class="form-control" >
                        </div>
                    </div>
                    <div class="col col-md-2">
                        <div class="form-group">
                            <select name="jenis" id="jenis" onchange="referedtgl()" class="form-control">
                                <option value="">-- Tipe --</option>
                                <option value="Purchase Order">Purchase Order</option>
                                <option value="Gudang Sample">Gudang Sample</option>
                                <option value="Retur Customer">Retur Customer</option>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                                <select name="vendor" onchange="referedtgl()" required class="form-control" id="vendor">
                                 <option value="">- Pilih Vendor -</option>
                                <!--<?php
                                    $varquery = "select * from m_vendor";
                                    $res = mysqli_query($con,$varquery);
                                    while($ds = mysqli_fetch_array($res)){
                                ?>
                                    <option value="<?php echo $ds['m_vendor_id'] ?>"><?php echo $ds['vendor_desc'] ?></option>
                                <?php
                                    }
                                ?> -->
                                </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <!-- fetch data here -->
        <b>
        <hr>
        <!-- <h3>Detail Item</h3> -->
        <div class="form-group">
        <!-- <form action="" method="post"> -->
        <div class="form-row">
            <?php
                $rand=rand();
                $_SESSION['rand']=$rand;
            ?>  
            <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" />
            <div class="col col-md-2">
                <label for="">Nama Barang</label>
                <select name="kodebarang" id="kodebrg" class="kodebrg">
                    <option value="">-- Item --</option>
                    <?php
                        $sel =mysqli_query($con,"select * from m_barang order by nama_barang");
                        while($dt = mysqli_fetch_array($sel)){
                    ?>
                    <option value="<?php echo $dt['m_barang_id']; ?>"><?php echo $dt['nama_barang']; ?></option>
                    <?php
                        }
                    ?>
                </select>
            </div>
            <div class="col col-md-2">
                <label for="">Batch</label>
                <input type="text" placeholder="Batch" id="batch" required name = "batch" value="" class="form-control">
            </div>
            <div class="col">
                <label for="">Tgl Expired</label>
                <input type="date"  name = "expired" required value="" id="expired" class="form-control">
            </div>
            <div class="col">
                <label for="">No Pack</label>
                <input type="text" placeholder="Pack" required id="nopack" name = "nopack" class="form-control">
            </div>
            <div class="col">
                <label for="">Pack Size</label>
                <input type="text" placeholder="Pack Size" id="size" required name = "packsize" class="form-control">
            </div>
        </div>
        <u><span id="prc" style="color:red;font-size:12px; margin-top:-10px"></span></u>
        <div class="form-row">
        
            <div class="col col-md-2">
                <label for="">QTY</label>
                <input type="text" placeholder="Qty" name = "qty" required id="qty"  class="form-control">
            </div>
            <div class="col col-md-2">
                <label for="">Kategori</label>
                <select name="gudang" class="form-control" id="kode">
                <option value="">- Kategori -</option>
                <?php
                    $varquery = "select * from m_kategori";
                    $res = mysqli_query($con,$varquery);
                    while($ds = mysqli_fetch_array($res)){
                ?>
                    <option value="<?php echo $ds['m_kategori_id'] ?>"><?php echo $ds['kategory_desc']; ?></option>
                <?php
                    }
                ?>
                </select>
            </div>
            <div class="col col-md-2">
                <label for="">Rak</label>
                <select name="gudangrak" required class="form-control" id="gudangrak">
                <option value="">- Rak -</option>
                </select>
            </div>
            <div class="col col-md-3">
                <label for="">Catatan</label>
                <input type="text" placeholder="Remark" id="notes" name = "catatan" class="form-control">
            </div>
            <div class="col col-md-2">
                <button class="btn btn-primary" id="addgr" style="margin-top:32px" name="trfbarang"><b>+</b></button>
                <span id="err" style="margin-top:110px; color:red"></span>
            </div>
            
        <!-- </div> -->
        
        </form>
        </div>
        </b>
        <!-- sampai sini -->
        <hr>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>
            <th>No</th>
            <th>Nama Item</th>
            <th>Principle</th>
            <th>Batch</th>
            <th>Pack Size</th>
            <th>No Pack</th>
            <th>Expired</th>
            <th>QTY</th>
            <th>Rak</th>
            <th style="text-align:center">Action</th>
            </tr>
        </thead>
        <tbody id="partial">
            <!-- <?php 
                $sel = "select a.t_gr_detail_temp_id, nama_barang,principle_desc,batch,packsize,nopack,Expired,qty,rak_desc from t_gr_detail_temp a
                inner join m_barang b on a.m_barang_id = b.m_barang_id
                inner join m_principle d on d.m_principle_id = b.m_principle_id
                inner join m_rak c on c.m_rak_id = a.m_rak_id
                where a.usercreated = '$m_user_id'";

                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
            ?>
            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['principle_desc']; ?></td>
            <td><?php echo $res['batch']; ?></td>
            <td><?php echo $res['packsize']; ?></td>
            <td><?php echo $res['nopack']; ?></td>
            <td><?php echo $res['Expired']; ?></td>
            <td><?php echo $res['qty']; ?></td>
            <td><?php echo $res['rak_desc']; ?></td>
            <td style="text-align:center">
                <a href="./?route=tambahgr&act=del&id=<?php echo $res['t_gr_detail_temp_id']; ?>"><button class="btn btn-danger">Hapus</button></a>
            </td>
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?> -->
        </tbody>
        </table>
    </div>
    <script>
       function refered(){
         var nopo_data = document.getElementById('nopo').value;
         var vno_data = document.getElementById('vno').value;
         document.getElementById('po').value = nopo_data ;
         document.getElementById('vnomer').value = vno_data ;
         
       }
       function referedtgl(){
         var nopo_data = document.getElementById('tglpo').value;
         var vendor_data = document.getElementById('vendor').value;
         var jns = document.getElementById('jenis').value;
         document.getElementById('potgl').value = nopo_data ;
         document.getElementById('povendor').value = vendor_data ;
         document.getElementById('jenispo').value = jns ;
       }
    </script>
    
    <form action="" method="POST" style="margin-left:20px">
        <input type="hidden" name="po" id="po">
        <input type="hidden" name="potgl" id="potgl">
        <input type="hidden" name="vnomer" value="<?php echo $ponomer ?>" id="vnomer">
        <input type="hidden" name="povendor" id="povendor">
        <input type="hidden" name="poprinciple" id="poprinciple">
        <input type="hidden" name="jenispo" id="jenispo">
        <table>
            <tr>
                <td>
                    <b><label for="">Driver</label></b>
                    <input type="text" class="form-control" name="driver">
                </td>
                <td>
                   <b><label for="">No Kendaraan</label></b> 
                    <input type="text" class="form-control" name="plat">
                </td>
                <td style="width:330px; padding-top:16px">
                   <b><label for="">Catatan</label></b> 
                    <textarea class="form-control" name="catatan"> </textarea>
                </td>
            </tr>
        </table>
        <br>
        <button class="btn btn-success" name="konfirmasi">Konfirmasi Penerimaan</button>
    </form>
   </div>
</div>
<script>
    $(".kodebrg").chosen();
    // $("#vendor").chosen();
    $('#kodebrg').on('change', function(e) {
        var kodebarang = $("#kodebrg").val();
        $.ajax({
            url:'pages/Receiptment/ajaxData.php',
            method : 'post',
            data : 'kodebarang= ' + kodebarang 
        }).done(function(vl){
            console.log(vl)
            $("#prc").text(vl);
        })
    });
</script>