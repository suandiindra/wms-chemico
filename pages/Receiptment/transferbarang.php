<?php
    $m_user_id = $_SESSION['user_id'];
    $t_po_id = $_GET['t_po_id'];
    $nopo = $_GET['nopo'];
    $batch = "";
    $expired = "";
    $nopack = "";
    $packsize = "";
    $qty = "";
    $gudangrak = "";
    $remark = "";
    $kodebarang = $_GET['kodebarang'];
    $vcek = $_GET['vcek'];
    $sel = "select (cast(qty as float)) as qty from t_po_detail where t_po_id = '$t_po_id' and m_barang_id = '$kodebarang'";
?>

<div class="container-fluid" id="container-wrapper">
    <div class="card-body">
        <h4>Transfer Gudang Nomor : <?php echo $vcek; ?></h4>
        <hr>
        <h5><b><?php echo ceknamabarang($con,$kodebarang)." (".cekbarangpo($con,$sel)." Kg)</br>"; ?></b></h5> 
        <form action="./pages/Receiptment/action_transferbarang.php" method="post">
        <div class="form-row">
            <input type="hidden" name = "kodebarang" value="<?php echo $kodebarang; ?>" class="form-control">
            <input type="hidden" name = "t_po_id" value="<?php echo $t_po_id; ?>" class="form-control">
            <input type="hidden" name = "vcek" value="<?php echo $vcek; ?>" class="form-control">
            <input type="hidden" name = "nopo" value="<?php echo $nopo; ?>" class="form-control">
            
            <div class="col">
                <input type="text" placeholder="Batch" name = "batch" value="<?php echo $batch; ?>" class="form-control">
            </div>
            <div class="col">
                <input type="date" placeholder="Tgl Expired" name = "expired" value="<?php echo $expired; ?>" class="form-control">
            </div>
            <div class="col">
                <input type="text" placeholder="Nomor Pack" name = "nopack" value="<?php echo $nopack; ?>" class="form-control">
            </div>
            <div class="col">
                <input type="text" placeholder="Pack Size" name = "packsize" value="<?php echo $packsize; ?>" class="form-control">
            </div>
            <div class="col">
                <input type="text" placeholder="Qty" name = "qty" value="<?php echo $qty; ?>" class="form-control">
            </div>
            <div class="col">
            <select name="gudangrak" class="form-control" id="sel1">
            <option value="">- Gudan & Rak -</option>
            <?php
                $varquery = "SELECT * from m_rak a
                inner join m_gudang b on a.m_gudang_id = b.m_gudang_id";
                $res = mysqli_query($con,$varquery);
                while($ds = mysqli_fetch_array($res)){
            ?>
                <option value="<?php echo $ds['m_rak_id'] ?>"><?php echo "[".$ds['gudang_desc']."]-[".$ds['rak_desc']."]" ?></option>
            <?php
                }
            ?>
            </select>
                <!-- <input type="text" placeholder="Gudang Rak" name = "$gudangrak" value="<?php echo $gudangrak; ?>" class="form-control"> -->
            </div>
            <div class="col col-md-3">
                <input type="text" placeholder="Remark" name = "remark" value="<?php echo $remark; ?>" class="form-control">
            </div>
            <div class="col col-md-1">
                <button class="btn btn-primary" name="trfbarang"><b>+</b></button>
            </div>
        </div>
        </form>
        <br><br>
        <table class="table table-hover">
        <thead>
            <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Barang</th>
            <th scope="col">Batch</th>
            <th scope="col">Expired</th>
            <th scope="col">No. Pack</th>
            <th scope="col">Pack Size</th>
            <th scope="col">Gudang</th>
            <th scope="col">Rak</th>
            <th scope="col">QTY</th>
            <th scope="col">Satuan</th>
            <th scope="col">Remark</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <?php
            $sel = "select t_gr_detail_temp_id,a.t_po_id,Expired,a.m_barang_id,b.nama_barang,qty,d.gudang_desc,c.rak_desc
            ,batch,Expired,a.nopack,packsize,remark from t_gr_detail_temp a
            inner join m_barang b on a.m_barang_id = b.m_barang_id
            inner join m_rak c on c.m_rak_id = a.m_rak_id
            inner join m_gudang d on d.m_gudang_id = c.m_gudang_id 
            where a.m_barang_id = '$kodebarang' and a.t_po_id = '$t_po_id' and a.usercreated = '$m_user_id'";

            // echo $sel;
            $i = 1;
            $total = 0;
            $res = mysqli_query($con,$sel);
            while($dt = mysqli_fetch_array($res)){
            $m_barang_id = $dt['m_barang_id'];
            $total = $total + $dt['qty'];
        ?>
        <tbody>
            <td><?php  echo $i; ?></td>
            <td><?php  echo $dt['nama_barang']; ?></td>
            <td><?php  echo $dt['batch']; ?></td>
            <td><?php  echo $dt['Expired']; ?></td>
            <td><?php  echo $dt['nopack']; ?></td>
            <td><?php  echo $dt['packsize']; ?></td>
            <td><?php  echo $dt['gudang_desc']; ?></td>
            <td><?php  echo $dt['rak_desc']; ?></td>
            <td><?php  echo $dt['qty']; ?></td>
            <td><?php  echo "Kg"; ?></td>
            <td><?php  echo $dt['remark']; ?></td>
            <td>
            <a href="./pages/Receiptment/action_transferbarang.php?act=del&t_po_id=<?php echo $t_po_id?>&kodebarang=<?php echo $m_barang_id?>&vcek=<?php echo $vcek; ?>&nopo=<?php echo $nopo; ?>&batch=<?php echo $dt['batch']; ?>">
            <button class="btn btn-danger"><b>X</b></button>
            </a>
            </td>
        </tbody>
        <?php
            $i = $i + 1;
            }
        ?>
        <tfoot>
            <td colspan="8" style="background-color:#F0FFFF">
                <center><b style="color:red">Total Penerimaan</b></center>
            </td>
            <td colspan="4">
               <b><?php echo $total." Kg"; ?></b>
            </td>
        </tfoot>
        </table>
        <a href="./?route=tambahgr&id=<?php echo $nopo; ?>&novceck=<?php echo $vcek ?>">
        <button class="btn btn-success" name="konfirmgr">Kembali</button>
        </a>
    </div>
</div>