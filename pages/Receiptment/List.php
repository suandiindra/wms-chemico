<?php
    $where = " where status = 1 ";
    if(isset($_POST['lihat'])){
      $date1 = $_POST['date1'];
      $date2 = $_POST['date2'];  
      $tipe  = $_POST['tipe'];

      if($date2){
        $where = $where." and tgl_gr between '$date1' and '$date2'";
      }

      if($tipe){
        $where = $where." and tipe_gr = '$tipe'";
      }
     
    }

    
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Penerimaan Barang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Reciepment</li>
    </ol>
    </div>
    <div>
        <div class="form-row col-md-12">
        <form action="" method="POST">
            <div class="col col-md-12">
               <div class="form-row">
                    <div class="col col-md-3">
                        <input type="date" name="date1" value="<?php echo $date1 ?>" class="form-control" placeholder="First name">
                    </div>
                    <div class="col col-md-3">
                        <input type="date" name="date2" value="<?php echo $date2; ?>" class="form-control" placeholder="First name">
                    </div>
                    <div class="col col-md-3">
                        <select name="tipe" id="" class="form-control">
                                <option value="">-- Semua --</option>
                                <option value="Purchase Order">Purchase Order</option>
                                <option value="Gudang Sample">Gudang Sample</option>
                                <option value="Retur Customer">Retur Customer</option>
                        </select>
                    </div>
                    <div class="col col-md-2">
                        <button class="btn btn-primary" name="lihat">Lihat</button>
                    </div>
               </div><br>
               
            </div>
        </form>
        <div class="col">
            <div class="form-row">
                <div class="col">
                    <a href="./index.php?route=tambahgr"><button class="btn btn-success float-left col-md-3">Terima Barang</button></a>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="card-body">
        <hr>
        <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No.</th>
                        <th>No. Visual Check</th>
                        <th>Tgl GR</th>
                        <th>Nomor PO</th>
                        <th>Tgl PO</th>
                        <th>Vendor/Cust</th>
                        <th>Total (Kg.)</th>
                        <th>Tipe Receiptment</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          
                          $sel = "select a.t_gr_id,a.visual_check_no,a.tgl_gr,c.nomor_po,c.tgl_po
                          ,ifnull(e.vendor_desc,customer_desc) as vendor_desc,c.status_po
                          ,sum(b.qty_gr) as qty_gr 
                            ,sum((d.qty)) as qty_po
                            ,tipe_gr
                            from t_gr a
                            inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
                            inner JOIN t_po c on c.t_po_id = a.t_po_id
                            inner join t_po_detail d on d.t_po_id = c.t_po_id
                            and d.m_barang_id = b.m_barang_id
                            left join m_vendor e on e.m_vendor_id = c.m_vendor_id
                            left join m_customer f on f.m_customer_id = c.m_vendor_id
                            $where
                            group by a.t_gr_id,a.visual_check_no,a.tgl_gr,c.nomor_po,c.tgl_po,ifnull(e.vendor_desc,customer_desc),c.status_po
                            order by a.createdate desc";
                          $result = mysqli_query($con,$sel);
                          $i = 1;
                          while($res = mysqli_fetch_array($result)){
                      ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $res['visual_check_no']; ?></td>
                        <td><?php echo $res['tgl_gr']; ?></td>
                        <td><?php echo $res['nomor_po']; ?></td>
                        <td><?php echo $res['tgl_po']; ?></td>
                        <td><?php echo $res['vendor_desc']; ?></td>
                        <td><?php echo number_format($res['qty_gr'], 1, '.', ''); ?></td>
                        <td><?php echo $res['tipe_gr']; ?></td>
                        <td style="text-align:center">
                            <!-- <button onclick="confirmation('<?php echo $res['t_po_id']; ?>')" class="btn btn-danger">Hapus</button>
                            <a href="./?route=editpo&act=edit&id=<?php echo $res['t_po_id']; ?>"><button class="btn btn-warning">Edit</button></a> -->
                            <a href="./?route=grdetail&id=<?php echo $res['t_gr_id']; ?>&vcek=<?php echo $res['visual_check_no']; ?>"><button class="btn btn-warning">Lihat</button></a>
                            <a href="./pages/Receiptment/printGR.php?id=<?php echo $res['t_gr_id']; ?>&vcek=<?php echo $res['visual_check_no']; ?>"><button class="btn btn-success">Cetak</button></a>
                        </td>
                      </tr>
                      <?php
                          $i =  $i + 1;
                          }
                      ?>
                    </tbody>
                  </table>
        </div>
    </div>
</div>

<script>
    function confirmation(delName){
    var del=confirm("Yakin Ingin menghapus PO ini..??");
    if (del==true){
        window.location.href="./pages/Transaksi PO/action.php?act=del&id="+delName;
    }
    return del;
}
</script>