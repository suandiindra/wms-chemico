<!DOCTYPE html>
<html>
<head>
  <title>PT. CHEMICO SURABAYA</title>
</head>
<style>
    @print { 
        @page :footer { 
            display: none
        } 
    
        @page :header { 
            display: none
        } 
    }
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<body onload="window.print()">
<div class="d-flex flex-row justify-content-center align-items-center" style="height: 70px;">
    <div class="p-2">
        <h4>VIRTUAL CHECK MATERIAL</h4>
    </div>
</div>
<?php
    include("../../utility/config.php");
    $id = $_GET['id'];
    $vcek = $_GET['vcek'];
    $sel = "select a.t_gr_id,d.nomor_po,visual_check_no,tgl_gr,e.vendor_desc,
    c.nama_barang, batch,expired,b.packsize,cast(qty_gr as float)qty_gr,remark,a.penerima
    from t_gr a
    inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
    inner join m_barang c on c.m_barang_id = b.m_barang_id
    inner join t_po d on d.t_po_id = a.t_po_id
    inner join m_vendor e on e.m_vendor_id = d.m_vendor_id
    where a.t_gr_id = '$id' ";

    // echo $sel;
    $res = mysqli_query($con,$sel);
    $dt = mysqli_fetch_array($res);
?>
<div style="float:right">
    FRM-WH-001 : Rev 04
</div>
<b style="margin-top:-120px"> PT. CHEMICO SURABAYA</b>
<div class="d-flex flex-row align-items-left" >
    <img src="../../img/chemico.jpeg" style="width:130px;margin-top:0px; margin-left:20px;height:100px" alt="">
</div>
<div>
<center><span >Number : <?php echo $dt['visual_check_no'] ?></span></center>
</div>
<br>
<!-- <hr> -->
<div class="form-row">
    <div class="col col-md-3">Date : <u> <?php echo $dt['tgl_gr']."__" ?> </u></div>
    <div class="col">Time : <u> <?php echo $dt['tgl_gr']."__" ?></u></div>
</div>
<div class="form-row">
    <div class="col">
        <table>
            <tr>
                <td>Supplier Name</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><u><?php echo $dt['vendor_desc'] ?></u></td>
            </tr>
            <tr>
                <td>PO Number</td>
                <td style="padding-right:25px;padding-left:20px">:</td>
                <td><u><?php echo $dt['nomor_po'] ?></u></td>
            </tr>
        </table>
    </div>
</div>
<div class="form-row" style="margin-top:25px">
    <table class="table table-bordered">
    <thead>
        <tr>
        <th scope="col">No</th>
        <th scope="col">V/X</th>
        <th scope="col">Description</th>
        <th scope="col">BATCH</th>
        <th scope="col">EXPIRED</th>
        <th scope="col">NO PACK</th>
        <th scope="col">PACKING SIZE</th>
        <th scope="col">QTY(Kg)</th>
        <th scope="col">Kemassan</th>
        <th scope="col">Remarks</th>
        </tr>
    </thead>
    <?php
        $sel = "select a.t_gr_id,d.nomor_po,visual_check_no,tgl_gr,e.vendor_desc,driver,platnomor,
        c.nama_barang, nopack,batch,expired,b.packsize,cast(qty_gr as float)qty_gr,remark,a.penerima
        from t_gr a
        inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
        inner join m_barang c on c.m_barang_id = b.m_barang_id
        inner join t_po d on d.t_po_id = a.t_po_id
        inner join m_vendor e on e.m_vendor_id = d.m_vendor_id
        where a.t_gr_id = '$id'";
    
        // echo $sel;
        
        $res1 = mysqli_query($con,$sel);
        // $dta = mysqli_fetch_array($res1);
        $i = 1;
        while($dt = mysqli_fetch_array($res1)){
    ?>    
    <tbody>
        <tr>
        <th scope="row"><?php echo $i; ?></th>
        <th></th>
        <td><?php echo $dt['nama_barang']; ?></td>
        <td><?php echo $dt['batch']; ?></td>
        <td><?php echo $dt['expired']; ?></td>
        <td><?php echo $dt['nopack']; ?></td>
        <td><?php echo $dt['packsize']; ?></td>
        <td><?php echo $dt['qty_gr']; ?></td>
        <td></td>
        <td></td>
        </tr>
    </tbody>
    <?php
        $i = $i +1;
        }
    ?> 
    </table> 
</div>
    <?php
        $res2 = mysqli_query($con,"select * from t_gr where t_gr_id = '$id'");

        $dtx = mysqli_fetch_array($res2);
    ?>
    <div style="margin-top:650px">
                <div class="form-row">
                    <div class="col col-md-2">
                    Driver
                    </div>
                    <div class="col" style="margin-left:10px">
                    <?php echo $dtx['driver'] ?>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col col-md-2">
                    Plat Number
                    </div>
                    <div class="col" style="margin-left:10px">
                    <?php echo $dtx['platnomor']?>
                    </div>
                </div>
                <br>
                <div class="form-row">
                    <div class="col col-md-2">
                    Remark
                    </div>
                    <div class="col" style="margin-left:10px">
                    __________________________________________________________________________________________________________________________
                    __________________________________________________________________________________________________________________________
                    __________________________________________________________________________________________________________________________
                    __________________________________________________________________________________________________________________________
                    __________________________________________________________________________________________________________________________
                    </div>
                </div>
                <div class="form-row">
                    <div class="col col-md-2"></div>
                    <div class="col col-md-4" style="margin-left:10px">
                        Diperiksa Oleh
                        <div style="margin-top:50px">
                        _________________
                        </div>
                    </div>
                    <div class="col col-md-4">
                        Mengetahui
                        <div style="margin-top:50px">
                        _________________
                        </div>
                    </div>
                </div>
    </div>

</body>