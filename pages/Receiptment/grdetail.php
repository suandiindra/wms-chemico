<?php
    $m_user_id  = $_SESSION['user_id'];
    $t_po_id = "";
    $act = "";
    $novceck  = "";
    $nopo = "";
    $tglpo = "";
    $vendor = "";
    $created = "";
    $grstatus = "";
    if(isset($_GET['id'])){
       $id = $_GET['id'];
       $sel = "select c.tgl_po,ifnull(d.vendor_desc,customer_desc)vendor_desc,c.careated_by,a.penerima,a.tgl_gr
       ,b.*,e.nama_barang,f.principle_desc
       from t_gr a
       inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
       inner join t_po c on c.t_po_id = a.t_po_id
       left join m_vendor d on d.m_vendor_id = c.m_vendor_id
       left join m_customer g on g.m_customer_id = c.m_vendor_id
       inner join m_barang e on e.m_barang_id = b.m_barang_id
       inner join m_principle f on f.m_principle_id = e.m_principle_id
       where a.t_gr_id = '$id'";
       $res = mysqli_query($con,$sel);
       $dt = mysqli_fetch_array($res);
       $tglpo = $dt['tgl_po'];
       $vendor = $dt['vendor_desc'];
       $tglgr = $dt['tgl_gr'];
       $created = $dt['careated_by'];
       $penerima = $dt['penerima'];

    }
?>
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Detail Penerimaan Barang</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item">Edit PO</li>
    </ol>
    </div>
    <div class="card-body">
        <b>
        <div class="form-group">
            <table class="padding-table-columns">
                <tr>
                    <td ><label for="">Tgl PO</label></td>
                    <td ><label for="">:</label></td>
                    <td ><label for=""><?php echo $tglpo; ?></label></td>
                </tr>
                <tr>
                    <td ><label for="">Vendor/Cust</label></td>
                    <td ><label for="">:</label></td>
                    <td ><label for=""><?php echo $vendor; ?></label></td>
                </tr>
                <tr>
                    <td ><label for="">Dibuat Oleh</label></td>
                    <td ><label for="">:</label></td>
                    <td ><label for=""><?php echo $created; ?></label></td>
                </tr>
                <tr>
                    <td ><label for="">Tgl GR</label></td>
                    <td ><label for="">:</label></td>
                    <td ><label for=""><?php echo $tglgr; ?></label></td>
                </tr>
                <tr>
                    <td ><label for="">Diterima Oleh</label></td>
                    <td ><label for="">:</label></td>
                    <td ><label for=""><?php echo $penerima; ?></label></td>
                </tr>
            </table>
        </div>
        </b>
        <!-- sampai sini -->
        <hr>
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
        <thead class="thead-light">
            <tr>-->
<!--            <th>No</th>-->
<!--            <th>Nama Item</th>-->
<!--            <th>Batch</th>-->
<!--            <th>Principle</th>-->
<!--            <th>QTY Terima</th>-->
<!--            <th>Satuan</th>-->
<!--        </tr>-->

            <tr>
            <th>No</th>
            <th>Nama Item</th>
            <th>Batch</th>
            <th>Expired</th>
            <th>No Pack</th>
            <th>Pack Size</th>
            <th>QTY Terima</th>
            <th>Satuan</th>
            <th>Principle</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
            ?>
            <tr>-->
<!--                        <td>--><?php //echo $i; ?><!--</td>-->
<!--                        <td>--><?php //echo $res['nama_barang']; ?><!--</td>-->
<!--                        <td>--><?php //echo $res['batch']; ?><!--</td>-->
<!--                        <td>--><?php //echo $res['principle_desc']; ?><!--</td>-->
<!--                        <td>--><?php //echo $res['qty_gr']; ?><!--</td>-->
<!--                        <td>--><?php //echo "Kg" ?><!--</td>-->
<!--                    </tr>-->

            <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $res['nama_barang']; ?></td>
            <td><?php echo $res['batch']; ?></td>
            <td><?php echo $res['expired']; ?></td>
            <td><?php echo $res['nopack']; ?></td>
            <td><?php echo $res['packsize']; ?></td>
            <td><?php echo format($res['qty_gr']); ?></td>
            <td><?php echo "Kg" ?></td>
            <td><?php echo $res['principle_desc']; ?></td>
            </tr>
            <?php
                $i =  $i + 1;
                }
            ?>
        </tbody>
        </table>
    </div>
    <a href="./?route=GR">
    <button class="btn btn-success">Kembali</button>
    </a>
</div>